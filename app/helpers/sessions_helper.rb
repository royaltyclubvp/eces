module SessionsHelper
  def log_in(participant)
    session[:participant_id] = participant.id
    session[:participation_id] = participant.participation_id
  end

  def current_participant
    @current_participant ||= Participant.find_by(id: session[:participant_id])
  end

  def logged_in?
    session.has_key?("participant_id")
  end

  def log_out
    session.delete(:participant_id)
    session.delete(:participation_id)
    @current_participant = nil
  end
end
