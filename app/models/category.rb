# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Category < ActiveRecord::Base
  has_many :products, inverse_of: :category, dependent: :destroy 
  has_many :experiment_group_categories
  has_many :experiment_groups, through: :experiment_group_categories
  has_many :experiment_category_views, inverse_of: :category
  has_many :experiment_product_detail_views, inverse_of: :category
end
