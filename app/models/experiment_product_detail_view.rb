# == Schema Information
#
# Table name: experiment_product_detail_views
#
#  id             :integer          not null, primary key
#  product_id     :integer
#  participant_id :integer
#  experiment_id  :integer
#  view_time      :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class ExperimentProductDetailView < ActiveRecord::Base
    # Belongs to Product, Participant & Experiment
    belongs_to :product, inverse_of: :experiment_product_detail_views
    belongs_to :participant, inverse_of: :experiment_product_detail_views
    belongs_to :experiment, inverse_of: :experiment_product_detail_views
end
