# == Schema Information
#
# Table name: experiments
#
#  id                       :integer          not null, primary key
#  name                     :string
#  available                :boolean
#  survey_link              :string
#  experiment_group_counter :integer          default(0)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  completion_message       :string
#  show_survey_link         :boolean
#

class Experiment < ActiveRecord::Base
  # Scopes
  scope :available, -> { where(available: true)}

  has_many :experiment_groups, autosave: true, dependent: :delete_all

  # Has Many Experiment Results
  has_many :experiment_results, -> { order('created_at ASC') }, inverse_of: :experiment

  # Has Many Clicks
  has_many :experiment_clicks,  -> { order('created_at ASC') },inverse_of: :experiment

  # Has Many Product Placements
  has_many :experiment_product_placements, -> { order('created_at ASC') }, inverse_of: :experiment

  # Has Many Product Selects
  has_many :experiment_product_selects, -> { order('created_at ASC') }, inverse_of: :experiment

  # Has Many Product Views
  has_many :experiment_product_views, -> { order('created_at ASC') }, inverse_of: :experiment

  # Has Many Experiment Category Views
  has_many :experiment_category_views, -> { order('created_at ASC') }, inverse_of: :experiment 

  # Has Many Experiment Product Detail Views
  has_many :experiment_product_detail_views, -> { order('created_at ASC') }, inverse_of: :experiment

  # Has Many Purchases
  has_many :experiment_purchases, -> { order('created_at ASC') }, inverse_of: :experiment

  # Has Many Participants
  has_many :participants, -> { order('created_at ASC') }, inverse_of: :experiment

  validates :name, presence: true
end
