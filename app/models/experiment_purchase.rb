# == Schema Information
#
# Table name: experiment_purchases
#
#  id             :integer          not null, primary key
#  participant_id :integer
#  product_id     :integer
#  quantity       :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  experiment_id  :integer
#
# Indexes
#
#  index_experiment_purchases_on_experiment_id  (experiment_id)
#

class ExperimentPurchase < ActiveRecord::Base
  # Belongs to Product & Participant
  belongs_to :product, inverse_of: :experiment_purchases
  belongs_to :participant, inverse_of: :experiment_purchases

  # Belongs to Experiment
  belongs_to :experiment, inverse_of: :experiment_purchases
end
