# == Schema Information
#
# Table name: experiment_clicks
#
#  id             :integer          not null, primary key
#  participant_id :integer
#  click_type     :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  experiment_id  :integer
#  click_time     :integer
#
# Indexes
#
#  index_experiment_clicks_on_experiment_id  (experiment_id)
#

class ExperimentClick < ActiveRecord::Base
  # Click belongs to a single participant
  belongs_to :participant, inverse_of: :experiment_clicks
  # Click belongs to an experiment
  belongs_to :experiment, inverse_of: :experiment_clicks
end
