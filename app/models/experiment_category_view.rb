# == Schema Information
#
# Table name: experiment_category_views
#
#  id             :integer          not null, primary key
#  category_id    :integer
#  participant_id :integer
#  experiment_id  :integer
#  view_time      :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class ExperimentCategoryView < ActiveRecord::Base
    # Belongs to Category, Participant & Experiment
    belongs_to :category, inverse_of: :experiment_category_views
    belongs_to :experiment, inverse_of: :experiment_category_views
    belongs_to :participant, inverse_of: :experiment_category_views
end
