# == Schema Information
#
# Table name: experiment_product_views
#
#  id             :integer          not null, primary key
#  product_id     :integer
#  participant_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  experiment_id  :integer
#  view_time      :integer
#
# Indexes
#
#  index_experiment_product_views_on_experiment_id  (experiment_id)
#

class ExperimentProductView < ActiveRecord::Base
  # Belongs to Product & Participant
  belongs_to :product, inverse_of: :experiment_product_views
  belongs_to :participant, inverse_of: :experiment_product_views

  # Belongs to Experiment
  belongs_to :experiment, inverse_of: :experiment_product_views
end
