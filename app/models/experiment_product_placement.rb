# == Schema Information
#
# Table name: experiment_product_placements
#
#  id             :integer          not null, primary key
#  product_id     :integer
#  grid_location  :string
#  participant_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  experiment_id  :integer
#
# Indexes
#
#  index_experiment_product_placements_on_experiment_id  (experiment_id)
#

class ExperimentProductPlacement < ActiveRecord::Base
  # Belongs to Product & Participant
  belongs_to :product, inverse_of: :experiment_product_placements
  belongs_to :participant, inverse_of: :experiment_product_placements

  # Belongs to an Experiment
  belongs_to :experiment, inverse_of: :experiment_product_placements
end
