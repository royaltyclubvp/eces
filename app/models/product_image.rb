# == Schema Information
#
# Table name: product_images
#
#  id         :integer          not null, primary key
#  filename   :string
#  product_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  resize     :boolean          default(FALSE)
#

class ProductImage < ActiveRecord::Base
  belongs_to :product, inverse_of: :product_images

  mount_uploader :filename, ProductImageUploader
end
