# == Schema Information
#
# Table name: experiment_groups
#
#  id                                          :integer          not null, primary key
#  group_no                                    :integer
#  experiment_id                               :integer
#  step_by_step                                :boolean
#  products_per_page                           :integer
#  price_location                              :string
#  add_to_basket_location                      :string
#  detail_page                                 :boolean
#  basket_order                                :string
#  category_selection_limit                    :integer
#  out_of_stock                                :boolean
#  randomization_of_products                   :boolean
#  cart_position                               :string
#  created_at                                  :datetime         not null
#  updated_at                                  :datetime         not null
#  background_type                             :string
#  background_color                            :string
#  background_image                            :string
#  logo_filename                               :string
#  front_image_1                               :string
#  front_image_2                               :string
#  front_image_3                               :string
#  slogan                                      :string
#  out_of_stock_per_category                   :boolean
#  out_of_stock_message                        :string
#  step_by_step_inappropriate_category_message :string
#  force_cart_removal                          :boolean          default(FALSE)
#  force_cart_removal_message                  :string
#  category_limit_message                      :string
#  font_color                                  :string
#  step_by_step_no_selection_message           :string
#  inappropriate_start_point_message           :string
#

class ExperimentGroup < ActiveRecord::Base
  belongs_to :experiment, inverse_of: :experiment_groups
  has_many :experiment_group_categories, -> { order(:order_no) }
  has_many :categories, through: :experiment_group_categories
  has_many :participants, inverse_of: :experiment_group
  has_many :experiment_results, inverse_of: :experiment_group
  mount_uploader :background_image, BackgroundImageUploader
  mount_uploader :logo_filename, LogoFilenameUploader
  mount_uploader :front_image_1, FrontImageUploader
  mount_uploader :front_image_2, FrontImageUploader
  mount_uploader :front_image_3, FrontImageUploader

  def as_json(options={})
    super(include: [:categories])
  end
end
