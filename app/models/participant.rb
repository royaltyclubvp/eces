# == Schema Information
#
# Table name: participants
#
#  id                  :integer          not null, primary key
#  participation_id    :string
#  experiment_group_id :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  experiment_id       :integer
#  access_token        :string
#
# Indexes
#
#  index_participants_on_access_token   (access_token)
#  index_participants_on_experiment_id  (experiment_id)
#

class Participant < ActiveRecord::Base
  before_create :generate_access_token

  # Participant belongs to an Experiment
  belongs_to :experiment, inverse_of: :participants

  # Participant belongs to an Experiment Group
  belongs_to :experiment_group, inverse_of: :participants

  # Participant is shown Products in a Particular Order
  has_many :experiment_product_placements, inverse_of: :participant
  has_many :placed_products, through: :experiment_product_placements, source: :product

  # Participant selects products during experiment
  has_many :experiment_product_selects, inverse_of: :participant
  has_many :selected_products, class_name: "Product", through: :experiment_product_selects

  # Participant views products during experiment
  has_many :experiment_product_views, inverse_of: :participant
  has_many :viewed_products, class_name: "Product", through: :experiment_product_views

  # Participant purchases products at the end of experiment
  has_many :experiment_purchases, inverse_of: :participant
  has_many :purchases, class_name: "Product", through: :experiment_purchases

  # Participant clicks on interface many times during experiment
  has_many :experiment_clicks, inverse_of: :participant

  # Experiment Category Views
  has_many :experiment_category_views, inverse_of: :participant

  # Experiment Product Detail Views
  has_many :experiment_product_detail_views, inverse_of: :participant

  # Participant has one result set
  has_one :experiment_result, inverse_of: :participant

  private

    def generate_access_token
      begin
        self.access_token = SecureRandom.hex
      end while self.class.exists?(access_token: access_token)
    end
end
