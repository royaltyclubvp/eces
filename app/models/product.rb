# == Schema Information
#
# Table name: products
#
#  id                    :integer          not null, primary key
#  title                 :string
#  price_cents           :integer
#  description           :text
#  category_id           :integer
#  main_product_image_id :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class Product < ActiveRecord::Base
  # Product has many images
  has_many :product_images, inverse_of: :product,  autosave: true, dependent: :delete_all

  # Product belongs to a product category
  belongs_to :category, inverse_of: :products

  # Product has been placed in many experiments
  has_many :experiment_product_placements, inverse_of: :product

  # Product has been selected in many experiments
  has_many :experiment_product_selects, inverse_of: :product

  # Product has been viewed in many experiments
  has_many :experiment_product_views, inverse_of: :product

  # Product has been purchased in many experiments
  has_many :experiment_purchases, inverse_of: :product

  # Apply Monetize gem to Price Column
  monetize :price_cents

  validates :title, presence: true

  def as_json(options={})
    super(include: [:product_images])
  end
end
