# == Schema Information
#
# Table name: experiment_group_categories
#
#  id                  :integer          not null, primary key
#  experiment_group_id :integer
#  category_id         :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  order_no            :integer
#

class ExperimentGroupCategory < ActiveRecord::Base
  belongs_to :experiment_group, inverse_of: :experiment_group_categories
  belongs_to :category, inverse_of: :experiment_group_categories
end
