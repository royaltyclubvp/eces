# == Schema Information
#
# Table name: experiment_product_selects
#
#  id             :integer          not null, primary key
#  product_id     :integer
#  participant_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  experiment_id  :integer
#  select_time    :integer
#
# Indexes
#
#  index_experiment_product_selects_on_experiment_id  (experiment_id)
#

class ExperimentProductSelect < ActiveRecord::Base
  # Belongs to Product & Participant
  belongs_to :product, inverse_of: :experiment_product_selects
  belongs_to :participant, inverse_of: :experiment_product_selects

  # Belongs to Experiment
  belongs_to :experiment, inverse_of: :experiment_product_selects
end
