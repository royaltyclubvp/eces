# == Schema Information
#
# Table name: experiment_results
#
#  id                  :integer          not null, primary key
#  experiment_id       :integer
#  experiment_group_id :integer
#  participant_id      :integer
#  completed           :boolean
#  completion_time     :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class ExperimentResult < ActiveRecord::Base
  # Result belongs to participant
  belongs_to :participant, inverse_of: :experiment_result
  # Result belongs to an experiment
  belongs_to :experiment, inverse_of: :experiment_results
  # Result belongs to an Experiment Group
  belongs_to :experiment_group, inverse_of: :experiment_results
end
