class ExperimentResultsController < ApplicationController
  before_action :logged_in_user
  def create
    result = ExperimentResult.find_by(participant_id: current_participant.id)
    result.completed = true
    result.completion_time = params[:completion_time].round
    result.save
  end
end
