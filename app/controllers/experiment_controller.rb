class ExperimentController < ApplicationController
  layout 'application'


  def home
    @experiment_settings = ExperimentGroup.includes(:categories).find_by_id(current_participant.experiment_group)
    @experiment = Experiment.find(@experiment_settings.experiment_id)
  end

  def home2

  end

  def index
    @experiments = Experiment.includes(experiment_groups: [experiment_group_categories: [:category]]).available
    respond_to do |format|
      format.json {
          render :json => @experiments.to_json(:include => {
              :experiment_groups => {
                  :include => {
                      :experiment_group_categories => {
                          :include => :category
                      }
                  }
              }
          })
      }
    end
  end

end
