class ExperimentClicksController < ApplicationController

  def create
      click_params = {
          participant_id: params[:participant_id],
          click_type: params[:click_type],
          experiment_id: params[:experiment_id],
          click_time: params[:click_time]
      }
      ExperimentClick.create(click_params)
      render :json => {response: 'success'}
  end
end
