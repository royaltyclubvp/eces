class ExperimentCategoryViewsController < ApplicationController
    def create
        view_params = {
            category_id: params[:category_id],
            participant_id: params[:participant_id],
            experiment_id: params[:experiment_id],
            view_time: params[:view_time]
        }
        ExperimentCategoryView.create(view_params)
        render :json => {response: 'success'}
    end
end
