class Admin::ProductImagesController < ApplicationController
  layout "admin"
  before_action :authenticate_admin!

  def new
    @product_image = ProductImage.new
    @product = Product.find(params[:product_id])
  end

  def create
    product_image = ProductImage.new({resize: params[:product_image][:resize]})
    if product_image.update_attributes(image_params)
      flash[:success] = 'Image Uploaded!'
      product = Product.find(product_image.product_id)
      if product.product_images.count == 1
        product.main_product_image_id = product_image.id
        product.save
      end
      redirect_to admin_product_path(product_image.product)
    else
      flash[:error] = 'There was an error in attempting to add a new product image. Please try again. If the problem persists, please contact your administrator.'
      redirect_to new_admin_product_product_image(params[:product_id])
    end
  end

  def edit
    @product_image = ProductImage.find(params[:id])
  end

  def update
    @product_image = ProductImage.find(params[:id])
    @product_image.update(image_params)
    if @product_image.save
      redirect_to admin_product_path @product_image.product
    else
      flash[:error] = 'There was an error in attempting to change the product image. Please try again. If the problem persists, please contact your administrator.'
      render 'edit'
    end
  end

  def destroy
    product_image = ProductImage.find(params[:id])
    if product_image.destroy
      flash[:success] = 'Image successfully deleted'
      redirect_to admin_product_path(product_image.product)
    else
      flash[:error] = 'There was an error in attempting to delete the product image. Please try again. If the problem persists, please contact your administrator.'
      redirect_to admin_product_path(product_image.product)
    end
  end

  def make_main_image
    product = Product.find(params[:product_id])
    product.main_product_image_id = params[:product_image_id]
    if product.save
      redirect_to admin_product_path product
    else
      flash[:error] = 'There was an error in attempting to change the main product image. Please try again. If the problem persists, please contact your administrator.'
      redirect_to admin_product_path product
    end
  end

  private
    def image_params
      params.require(:product_image).permit(:filename, :product_id, :resize)
    end
end
