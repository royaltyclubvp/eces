class Admin::ExperimentGroupCategoriesController < ApplicationController
    before_action :authenticate_admin!

    def save_group_category_order
        params[:categories].each_with_index do |category, index|
            ExperimentGroupCategory.find_by(id: category[1][:experiment_group_category_id].to_i).update_attributes({order_no: category[1][:order_no].to_i})
        end
        respond_to do |format|
            format.json {
                render :json => {success: 'true'}
            }
        end
    end
end
