class Admin::ExperimentResultsController < ApplicationController
  layout "admin"
  before_action :authenticate_admin!

  def home
    @experiments = Experiment.all
  end

  def index
    @experiment = Experiment.includes(experiment_clicks: [
        participant: [:experiment_group]], experiment_product_placements: [:product, participant: [:experiment_group]], experiment_product_selects: [participant: [:experiment_group]], experiment_product_views: [participant: [:experiment_group]], experiment_category_views: [participant: [:experiment_group]], experiment_product_detail_views: [participant: [:experiment_group]], experiment_purchases: [participant: [:experiment_group]], experiment_results: [:experiment, :experiment_group], participants: [:experiment_group, :experiment_result], experiment_groups: [experiment_group_categories: [category: [:products]]]).find(params[:id])
    respond_to do |format|
      format.html
      format.xlsx {
        response.headers['Content-Disposition'] = 'attachment; filename="results.xlsx"'
      }
    end
  end

  def destroy
      experiment = Experiment.includes(:experiment_results).find(params[:id])
      if experiment.experiment_results.destroy_all
          flash[:success] = 'The results for this experiment have been deleted'
          redirect_to admin_experiment_results_home_path
      else
          flash[:error] = 'There was a problem attempting to delete the experiment results. Please try again. If the problem persists, please contact your administrator'
          redirect_to admin_experiment_results_home_path
      end
  end

  def purchases
    @purchases = ExperimentPurchase.where(participant_id: params[:id])
  end
end
