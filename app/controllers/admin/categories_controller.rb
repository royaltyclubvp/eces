class Admin::CategoriesController < ApplicationController
  layout "admin"
  before_action :authenticate_admin!

  def index
    @categories = Category.all
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      flash[:success] = "The category, #{@category.name}, was successfully created"
      redirect_to admin_categories_path
    else
      flash[:error] = 'There was an error in attempting to add a new category. Please try again. If the problem persists, please contact your administrator.'
      redirect_to new_admin_category_path
    end
  end

  def edit
    @category = Category.find(params[:id])
  end

  def update
    @category = Category.find(params[:id])
    if @category.update(category_params)
      redirect_to admin_categories_path
    else
      flash[:error] = 'There was an error in attempting to add a new category. Please try again. If the problem persists, please contact your administrator.'
      render 'edit'
    end
  end

  def destroy
    @category = Category.find(params[:id])
    if @category.destroy
      redirect_to admin_categories_path
    else
      flash[:error] = 'There was an error in attempting to delete the category. Please try again. If the problem persists, please contact your administrator.'
      redirect_to admin_categories_path
    end
  end

  private
    def category_params
      params.require(:category).permit(:name)
    end
end
