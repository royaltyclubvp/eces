class Admin::ExperimentGroupsController < ApplicationController
  layout "admin"
  before_action :authenticate_admin!

  def new
    @experiment = Experiment.find(params[:experiment_id])
    @experiment_group = ExperimentGroup.new
  end

  def index
    @experiment_groups = ExperimentGroup.where(experiment_id: params[:experiment_id])
  end

  def add_to_experiment
    experiment = Experiment.find(params[:experiment_id])
    @experiment_group = ExperimentGroup.new
    @experiment_group.experiment = experiment
    @experiment_group.group_no = (experiment.experiment_groups.count + 1)
  end

  def create
    experiment_group = ExperimentGroup.new(experiment_params)
    if experiment_group.save
      flash[:success] = 'The experiment group was successfully created'
      redirect_to admin_experiment_group_path(experiment_group)
    else
      flash[:error] = 'There was a problem adding an experimental group. Please try again. If the problem persists, please contact your administrator.'
      redirect_to new_admin_experiment_experiment_group_path(params[:experiment_id])
    end
  end

  def show
    @experiment_group = ExperimentGroup.includes(experiment_group_categories: [:category]).find(params[:id])
    @categories = Category.all
  end

  def edit
    @experiment_group = ExperimentGroup.find(params[:id])
  end

  def update
    @experiment_group = ExperimentGroup.find(params[:id])
    if @experiment_group.update(experiment_params)
      redirect_to admin_experiment_group_path @experiment_group
    else
      flash[:error] = 'There has been an error in attempting to update this experimental group. Please try again. If the problem persists, please contact your administrator.'
      redirect_to admin_experiment_group_path @experiment_group
    end
  end

  def destroy
    experiment_group = ExperimentGroup.find(params[:id])
    if experiment_group.destroy
      flash[:success] = 'Group successfully deleted'
      redirect_to admin_experiment_path(experiment_group.experiment)
    else
      flash[:error] = 'There has been an error in attempting to delete this experimental group. Please try again. If the problem persists, please contact your administrator.'
      redirect_to admin_experiment_experiment_group_path(experiment_group.experiment, experiment_group)
    end
  end

  def add_all_categories
    experiment_group = ExperimentGroup.find(params[:id])
    categories = Category.all
    experiment_group.experiment_group_categories.clear
    currentCategories = experiment_group.experiment_group_categories.size
    if currentCategories.nil?
        currentCategories = 0
    end
    categories.each do |category|
        currentCategories = currentCategories + 1
        experiment_group.experiment_group_categories << ExperimentGroupCategory.new({category_id: category.id, order_no: currentCategories})
    end
    if experiment_group.save
      redirect_to admin_experiment_group_path experiment_group
    else
      flash[:error] = 'There has been an error in attempting to add the categories to the group. Please try again. If the problem persists, please contact your administrator.'
      redirect_to admin_experiment_group_path experiment_group
    end
  end

  def delete_all_categories
    experiment_group = ExperimentGroup.find(params[:id])
    experiment_group.experiment_group_categories.clear
    if experiment_group.save
      redirect_to admin_experiment_group_path experiment_group
    else
      flash[:error] = 'There has been an error in attempting to delete the categories from the group. Please try again. If the problem persists, please contact your administrator.'
      redirect_to admin_experiment_group_path experiment_group
    end
  end

  def add_category
    experiment_group = ExperimentGroup.find(params[:group_id])
    experiment_group_category = ExperimentGroupCategory.new({experiment_group_id: params[:group_id], category_id: params[:category_id]})
    currentCategories = experiment_group.experiment_group_categories.size
    if currentCategories.nil?
        experiment_group_category.order_no = 1
    else
        experiment_group_category.order_no = currentCategories + 1
    end
    if experiment_group_category.save
      redirect_to admin_experiment_group_path experiment_group
    else
      flash[:error] = 'There has been an error in attempting to add the category to the group. Please try again. If the problem persists, please contact your administrator.'
      redirect_to admin_experiment_group_path experiment_group
    end
  end

  def destroy_category
    experiment_group = ExperimentGroup.find(params[:group_id])
    experiment_group_category = ExperimentGroupCategory.find_by(experiment_group_id: params[:group_id], category_id: params[:category_id])
    experiment_group.experiment_group_categories.delete(experiment_group_category)
    if experiment_group.save
      redirect_to admin_experiment_group_path experiment_group
    else
      flash[:error] = 'There has been an error in attempting to delete the category from the group. Please try again. If the problem persists, please contact your administrator.'
      redirect_to admin_experiment_group_path experiment_group
    end
  end

  private
    def experiment_params
      params.require(:experiment_group).permit(:group_no, :inappropriate_start_point_message, :slogan, :experiment_id, :step_by_step, :out_of_stock, :products_per_page, :price_location, :add_to_basket_location, :cart_position, :detail_page, :basket_order,
      :category_selection_limit, :randomization_of_products, :background_type, :background_color, :background_image, :logo_filename, :front_image_1, :front_image_2,
          :front_image_3, :out_of_stock_per_category, :out_of_stock_message, :force_cart_removal, :force_cart_removal_message, :category_limit_message, :step_by_step_inappropriate_category_message, :step_by_step_no_selection_message, :font_color)
    end
end
