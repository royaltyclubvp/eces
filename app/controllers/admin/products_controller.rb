class Admin::ProductsController < ApplicationController
  layout "admin"
  before_action :authenticate_admin!

  def new
    @product = Product.new
  end

  def create
    product = Product.new(product_params)
    if product.save
      flash[:success] = "The product, #{product.title}, was successfully created"
      redirect_to admin_products_path
    else
      flash[:error] = 'There was an error in attempting to add a new product. Please try again. If the problem persists, please contact your administrator.'
      redirect_to new_admin_product_path
    end
  end

  def show
    @product = Product.includes(:category).find(params[:id])
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    product = Product.find(params[:id])
    if product.update(product_params)
      flash[:success] = 'The product was successfully updated'
      redirect_to admin_product_path(product)
    else
      flash[:error] = 'There was an error in attempting to update the product. Please try again. If the problem persists, please contact your administrator.'
      redirect_to edit_admin_product_path(params[:id])
    end
  end

  def destroy
    @product = Product.find(params[:id])
    if @product.destroy
      redirect_to admin_category_products_path @product.category_id
    else
      flash[:error] = 'There was an error in attempting to delete the product. Please try again. If the problem persists, please contact your administrator.'
      redirect_to admin_product_path @product
    end
  end

  def index
    @products = Product.includes(:category).all
  end

  def index_by_category
    @products = Product.where(category_id: params[:category_id])
    @category = Category.find(params[:category_id])
  end

  def product_categories_home
    @categories = Category.all
  end

  private
    def product_params
      params.require(:product).permit(:title, :description, :price, :category_id)
    end
end
