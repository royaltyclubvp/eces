class Admin::ExperimentsController < ApplicationController
  layout "admin"
  before_action :authenticate_admin!

  def index
    @experiments = Experiment.all
  end

  def show
    @experiment = Experiment.includes(:experiment_groups).find(params[:id])
  end

  def new
    @experiment = Experiment.new
  end

  def create
    experiment = Experiment.new(experiment_params)
    if experiment.save
      flash[:success] = "Experiment, #{experiment.name}, was successfully created"
      redirect_to admin_experiment_path(experiment)
    else
      flash[:error] = "An error occurred. Please try again"
      redirect_to new_admin_experiment_path
    end
  end

  def edit
    @experiment = Experiment.find(params[:id])
  end

  def update
    experiment = Experiment.find_by({id: params[:id]})
    if experiment.update_attributes(experiment_params)
      redirect_to admin_experiment_path experiment
    end
  end

  def destroy
    @experiment = Experiment.find(params[:id])
    if @experiment.destroy
      flash[:success] = 'The experiment has been successfully deleted'
      redirect_to admin_experiments_path
    else
      flash[:error] = 'There was a problem in attempting to delete the experiment. Please try again. If the problem persists, contact your administrator'
      redirect_to admin_experiment_path @experiment
    end
  end

  def make_available
    experiment = Experiment.find(params[:id])
    experiment.available = true
    if experiment.save
      redirect_to admin_experiment_path experiment
    end
  end

  def make_unavailable
    experiment = Experiment.find(params[:id])
    experiment.available = false
    if experiment.save
      redirect_to admin_experiment_path experiment
    end
  end

  private
    def experiment_params
      params.require(:experiment).permit(:name, :available, :survey_link, :show_survey_link, :completion_message)
    end
end
