class ExperimentProductViewsController < ApplicationController

  def create
      view_params = {
          product_id: params[:product_id],
          participant_id: params[:participant_id],
          experiment_id: params[:experiment_id],
          view_time: params[:view_time]
      }
      ExperimentProductView.create(view_params)
  end
end
