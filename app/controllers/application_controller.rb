class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  include SessionsHelper

  private

    # Overwriting the sign_out redirect path method
    def after_sign_out_path_for(resource_or_scope)
      case resource_or_scope
        when :admin, Admin
          admin_root_path
        else
          super
      end
    end

    #Confirms a Logged In User
    def logged_in_user
      unless logged_in?
        redirect_to root_path
      end
    end

end
