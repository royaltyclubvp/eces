class ExperimentResultPackageController < ApplicationController
  def update
      participant = Participant.find_by({id: params[:id]})
      params[:purchases].each do |p|
          purchase = ExperimentPurchase.new
          purchase.participant = participant
          purchase.product_id = p[:id]
          purchase.quantity = 1
          purchase.experiment_id = params[:experiment_id]
          participant.experiment_purchases << purchase
      end
      result = ExperimentResult.find_by({participant_id: params[:id]})
      result.completion_time = params[:results][:completion_time]
      result.completed = true
      result.save
      participant.save
      render :json => {response: 'success'}
  end
end
