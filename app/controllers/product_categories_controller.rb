class ProductCategoriesController < ApplicationController

  def show
      # Retrieve Products According to Randomization Variable
      if params[:randomization] == 'true'
          @products = Product.includes(:product_images).where(category_id: params[:id]).order("RANDOM()")
      else
          @products = Product.includes(:product_images).where(category_id: params[:id])
      end
      # Retrieve Participant Record By Access Token Parameter
      participant = Participant.includes(:experiment_group, :experiment).find_by({access_token: params[:access_token]})
      unless participant.nil?
          # Initiate Variables
          row = 1
          column = 1
          column_max = 4
          # Set Page Counter if Products Per Page is Set
          unless participant.experiment_group.products_per_page.nil?
              page_counter = 1
          end
          @products.each_with_index do |p, i|
              placement = ExperimentProductPlacement.new
              placement.product = p
              #if participant.experiment_group.products_per_page.nil?
                #  placement.grid_location =  'P1' + '.' + row.to_s + '.' + column.to_s
              #else
                #  placement.grid_location = 'P' + page_counter.to_s + '.' + row.to_s + '.' + #column.to_s
              #end
              placement.grid_location = row.to_s + '.' + column.to_s
              placement.experiment = participant.experiment
              participant.experiment_product_placements << placement
              # If Last Product In Row According To Limit, Advance Row Counter & Reset Column Number; Else, just Advance Column Counter
              if ((i+1) % column_max) == 0
                  row += 1
                  column = 1
              else
                  column += 1
              end
              # If Last Product in Page According to Limit, Advance Page Counter & Reset Row & Column Counters
              unless participant.experiment_group.products_per_page.nil?
                  if ((i+1) % participant.experiment_group.products_per_page) == 0
                      page_counter += 1
                      row = 1
                      column = 1
                  end
              end
          end
          participant.save
      end
      respond_to do |format|
          format.json {
              if participant.nil?
                  render :json => {error: 'PARTICIPANT_INVALID'}
              else
                  render :json => @products
              end
          }
      end
  end

  def index
    if params[:randomization] == 'true'
      @products = Product.includes(:product_images).where(category_id: params[:category]).order("RANDOM()")
    else
      @products = Product.includes(:product_images).where(category_id: params[:category])
    end
    row = 1
    column_max = 4
    column = 1
    participant = current_participant
    @products.each_with_index do |product, index|
      product_placement = ExperimentProductPlacement.new
      product_placement.product = product
      product_placement.grid_location = row.to_s + '.' + column.to_s
      product_placement.experiment = current_participant.experiment_group.experiment
      participant.experiment_product_placements << product_placement
      if ((index+1) % column_max) == 0
        row += 1
        column = 1
      else
        column += 1
      end
      participant.save
    end
    render :json => @products
  end
end
