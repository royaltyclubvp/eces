class SessionsController < ApplicationController
  layout 'application'
  before_action :logged_in_user, only: [:select_experiment, :assign_experiment_group]

  def new
    @participant = Participant.new
  end

  def select_experiment
    @experiments = Experiment.available
    if @experiments.count == 1
      redirect_to assign_to_group_path(@experiments.first)
    end
  end

  def assign_experiment_group
    current_participant.experiment_id = params[:experiment_id]
    experiment_groups = ExperimentGroup.where(experiment_id: params[:experiment_id])
    if experiment_groups.count > 1
      experiment_groups = experiment_groups.sort { |a, b| a.participants.count <=> b.participants.count}
      current_participant.experiment_group = experiment_groups.first
    elsif experiment_groups.size == 1
      current_participant.experiment_group = experiment_groups.first
    end
    current_participant.save
    ExperimentResult.create({experiment_id: current_participant.experiment_group.experiment_id, experiment_group_id: current_participant.experiment_group_id, participant_id: current_participant.id, completed: false});
    redirect_to experiment_home_path
  end

  def create
    @participant = Participant.new(participant_params)
    if @participant.save
      log_in @participant
      redirect_to select_experiment_path
    else
      flash[:error] = 'There was an error while attempting to register. Please try again. If the problem persists, please contact an administrator.'
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_path
  end

  private
    def participant_params
      params.require(:participant).permit(:participation_id, :experiment_group)
    end
end
