class ExperimentProductSelectsController < ApplicationController

  def create
      select_params = {
          product_id: params[:product_id],
          participant_id: params[:participant_id],
          experiment_id: params[:experiment_id],
          select_time: params[:select_time]
      }
      ExperimentProductSelect.create(select_params)
      render :json => {response: 'success'}
  end
end
