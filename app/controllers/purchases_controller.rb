class PurchasesController < ApplicationController
  before_action :logged_in_user

  def create
    ExperimentPurchase.create({product_id: params[:product_id], participant_id: current_participant.id, quantity: 1, experiment_id: current_participant.experiment_group.experiment_id})
  end
end
