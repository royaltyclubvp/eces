class ParticipantsController < ApplicationController
  def create
    participant = Participant.new({participation_id: params[:participation_id]})
    respond_to do |format|
      format.json {
        if participant.save
          render :json => participant
        else
          render :json => {error: 'Participant could not be created'}
        end
      }
    end
  end

  def select_experiment
    participant = Participant.find_by({access_token: params[:access_token]})
    unless participant.nil?
      experiment = Experiment.includes(:experiment_groups).find_by({id: params[:experiment_id]})
      unless experiment.nil?
        # Assign Chosen Experiment to Participant Record
        participant.experiment = experiment
        no_of_groups = experiment.experiment_groups.size
        # If Multiple Experiment Groups, Assign to Group with Fewest Participants; If Single Group, Assign to Group
        if no_of_groups > 1
          groups = experiment.experiment_groups.sort { |a, b| a.participants.count <=> b.participants.count }
          participant.experiment_group = groups.first
        elsif no_of_groups == 1
          participant.experiment_group = experiment.experiment_groups.first
        end
      end
    end
    saved = participant.save
    # If Save is Successful, Create Experiment Result Record
    if saved
      ExperimentResult.create({
                                  experiment_id: participant.experiment_group.experiment_id,
                                  experiment_group_id: participant.experiment_group_id,
                                  participant_id: participant.id,
                                  completed: false
                              })
    end
    respond_to do |format|
      format.json {
        if participant.nil?
          render :json => {error: 'PARTICIPANT_INVALID'}
        elsif experiment.nil?
          render :json => {error: 'EXPERIMENT_INVALID'}
        elsif no_of_groups == 0
          render :json => {error: 'EXPERIMENT_GROUP_NOT_FOUND'}
        elsif not saved
          render :json => {error: 'GROUP_ASSIGNMENT_ERROR'}
        else
          render :json => {experiment_id: participant.experiment_id, experiment_group_id: participant.experiment_group_id}
        end
      }
    end
  end
end
