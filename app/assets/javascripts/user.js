//= require jquery
//= require jquery_ujs
//= require utilities
//= require jquery.visible
//= require jquery.waypoints
//= require bootstrap
//= require handlebars.runtime
//= require underscore
//= require backbone.dev
//= require backbone/app
//= require backbone/app_settings
//= require backbone/app_dictionary
//= require backbone/api
//= require backbone/handlebars_helpers
//= require_tree ./backbone/templates
//= require_tree ./backbone/models
//= require_tree ./backbone/collections
//= require_tree ./backbone/views
//= require_tree ./backbone/controllers
//= require_tree ./backbone/routers
//= require backbone/app_boot

$(document).ready(function() {
    App.init();
});
