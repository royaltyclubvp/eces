//= require jquery
//= require jquery_ujs
//= require jquery.minicolors
//= require bootstrap
//= require autoNumeric
//= require underscore
//= require backbone.dev
//= require handlebars.runtime
//= require_tree ./admin/templates
//= require admin/products
//= require admin/experiment_category_order

$(document).ready(function() {
    $('.color-picker').minicolors();
});
