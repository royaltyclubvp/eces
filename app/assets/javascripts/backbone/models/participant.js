App.Models.Participant = Backbone.Model.extend({
    urlRoot: '/participants/',

    initialize: function() {

    },

    getToken: function() {
        return this.get('access_token');
    },

    selectExperiment: function(experiment_id) {
        if (this.get('access_token')) {
            var scope = this;
            var select = $.ajax({
                url: apiUrl('select_experiment', scope.get('access_token')),
                method: 'POST',
                data: {experiment_id: experiment_id}
            });
            select.done(function(response) {
                if (response.error) {
                    Backbone.trigger('notification', App.Dictionary.Registration[response.error]);
                    scope.trigger('experiment_group_assignment_failed');
                } else {
                    scope.set({
                        experiment_id: response.experiment_id,
                        experiment_group_id: response.experiment_group_id
                    });
                    scope.trigger('experiment_group_assigned');
                    scope.experiment = App.collections.experiments.findWhere({id: response.experiment_id});
                    scope.experimentGroup = scope.experiment.groups.findWhere({id: response.experiment_group_id});
                    // Call Post-Setup Hook on Experiment Group
                    scope.experimentGroup.postSetupHook();
                    // Initialize Shopping Cart
                    scope.shoppingCart = new App.Collections.ShoppingCart();
                    // Initialize variable for out of stock check if experiment_wide
                    if (!scope.experimentGroup.get('out_of_stock_per_category')) {
                        scope.out_of_stock_triggered = false;
                    }
                    scope.experimentClicks = new App.Collections.ExperimentClicks();
                    scope.experimentSelects = new App.Collections.ExperimentSelects();
                    scope.experimentViews = new App.Collections.ExperimentViews();
                    scope.experimentCategoryViews = new App.Collections.ExperimentCategoryViews();
                    scope.experimentProductDetailViews = new App.Collections.ExperimentProductDetailViews();
                    scope.experimentResult = new App.Models.ExperimentResult();
                }
            });
            select.fail(function() {
                // Handle Non-200 Response
                // TODO Write Method to handle Non-200 Response
                scope.trigger('experiment_group_assignment_failed');
            });
        }
    },

    checkOutOfStock: function() {
        return this.out_of_stock_triggered;
    },

    disableOutOfStock: function() {
        this.out_of_stock_triggered = true;
    },

    finalizeResults: function() {
        var resultPackage = new App.Models.ExperimentResultPackage();
        resultPackage.set('id', this.get('id'));
        resultPackage.set('experiment_id', this.experiment.get('id'));
        resultPackage.set('purchases', this.shoppingCart.toJSON());
        //resultPackage.set('clicks', this.experimentClicks.toJSON());
        //resultPackage.set('selects', this.experimentSelects.toJSON());
        //resultPackage.set('views', this.experimentViews.toJSON());
        resultPackage.set('results', this.experimentResult.toJSON());
        resultPackage.save();
    }
});
