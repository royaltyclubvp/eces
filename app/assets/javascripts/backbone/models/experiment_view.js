App.Models.ExperimentView = Backbone.Model.extend({
    initialize: function() {
        this.set('view_time', Math.ceil((Date.now() - App.participant.experiment.getStartTime())/1000));
        this.set('participant_id', App.participant.get('id'));
        this.set('experiment_id', App.participant.get('experiment_id'));
    }
});
