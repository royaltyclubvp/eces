App.Models.ExperimentProductDetailView = Backbone.Model.extend({
    initialize: function() {
        this.set('participant_id', App.participant.get('id'));
        this.set('experiment_id', App.participant.get('experiment_id'));
        this.set('start_time', Math.ceil(Date.now()));
    },

    calculateViewTime: function() {
        if (this.get('start_time')) {
            this.set('view_time', (Date.now() - this.get('start_time'))/1000);
        }
        return this;
    }
});
