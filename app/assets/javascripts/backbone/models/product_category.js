App.Models.ProductCategory = Backbone.Model.extend({
    initialize: function() {
        this.set('id', this.get('category').id);
        this.set('slug', convertToSlug(this.get('category').name));
        this.set('name', this.get('category').name);
    },

    postSetupHook: function() {
        // Retrieve Category Products
        this.products = new App.Collections.ProductCatalogue(null, {url: apiUrl('category_products', this.get('id'), App.participant.getToken()), category: this});
        var random = App.participant.experimentGroup.get('randomization_of_products') == true ? 'true' : 'false';
        this.products.fetch({reset: true, data: {randomization: random}});
        // Set Out of Stock Trigger if Experiment Group Setting Per Category is Enabled
        if (App.participant.experimentGroup.get('out_of_stock_per_category')) {
            this.out_of_stock_triggered = false;
        }
        // Set Selection Counter
        this.categorySelectionCounter = 0;
        // Set View Items Container
        this.viewed = [];
    },

    addToViewed: function(product) {
        if ($.inArray(product, this.viewed) == -1) {
            this.viewed.push(product);
            App.participant.experimentViews.create({product_id: product});
        }
    },

    checkOutOfStock: function() {
        return this.out_of_stock_triggered;
    },

    disableOutOfStock: function() {
        this.out_of_stock_triggered = true;
    },

    checkLimit: function() {
        return this.categorySelectionCounter < App.participant.experimentGroup.get('category_selection_limit');
    },

    checkIfProductAdded: function() {
        // Also Return True if There is Only One Product In the Category and the Out of Stock Has Been Triggered, Or If The Category Has No Products
        return (this.categorySelectionCounter > 0 || (this.out_of_stock_triggered == true && this.products.length == 1) || this.products.length == 0);
    },

    incrementProductCounter: function() {
        this.categorySelectionCounter++;
    },

    decrementProductCounter: function() {
        this.categorySelectionCounter--;
    }
});
