App.Models.Experiment = Backbone.Model.extend({
    initialize: function() {
        this.groups = new App.Collections.ExperimentGroups();
        if (this.get('experiment_groups')) {
            this.groups.set(this.get('experiment_groups'));
        }
        this.on('change:experiment_groups', this.updateExperimentGroups);
    },

    updateExperimentGroups: function() {
        this.groups.reset(this.get('experiment_groups'));
    },

    setStartTime: function(time) {
        this.startTime = time;
    },

    getStartTime: function() {
        return this.startTime;
    }
});
