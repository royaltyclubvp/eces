App.Models.ProductImage = Backbone.Model.extend({
    initialize: function() {
        this.set('filename', this.get('filename').url);
    }
});