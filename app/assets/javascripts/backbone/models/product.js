App.Models.Product = Backbone.Model.extend({
    initialize: function() {
        this.set('slug', convertToSlug(this.get('title')));
        if (this.get('product_images').length) {
            this.images = new App.Collections.ImageCatalogue(this.get('product_images'));
        }
        if (this.get('main_product_image_id')) {
            this.set('main_image', this.images.findWhere({id: this.get('main_product_image_id')}).get('filename'));
        }
        // Set In Cart Status Default
        this.set('in_cart', false);
        // Set Stock Default
        this.set('in_stock', true);
        // Set Price to Dollar Format
        this.set('price', (this.get('price_cents')/100).toFixed(2));
        // Set Category
        this.category = this.collection.category;
        this.set('category_slug', this.collection.category.get('slug'));
    },

    addToCart: function() {
        this.set('in_cart', true);
    },

    removeFromCart: function() {
        this.set('in_cart', false);
    },

    removeFromStock: function() {
        this.set('in_stock', false);
    },

    addToStock: function() {
        this.set('in_stock', true);
    }
});
