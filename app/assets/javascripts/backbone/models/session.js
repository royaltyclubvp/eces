App.Models.Session = Backbone.Model.extend({
    initialize: function() {
        if(storageAvailable('localStorage') && localStorage.getItem('token') != null) {
            this.storage = localStorage;
            this.supportStorage = true;
        } else if(storageAvailable('sessionStorage')) {
            this.storage = sessionStorage;
            this.supportStorage = true;
        }
    },

    get: function(key) {
        if(this.supportStorage) {
            var data = this.storage.getItem(key);
            if(data && data[0] === '{') {
                return JSON.parse(data);
            } else {
                return data;
            }
        } else {
            return Backbone.Model.prototype.get.call(this, key);
        }
    },

    set: function(key, value) {
        if(this.supportStorage) {
            this.storage.setItem(key, value);
        } else {
            Backbone.Model.prototype.set.call(this, key, value);
        }
        return this;
    },

    unset: function(key) {
        if(this.supportStorage) {
            this.storage.removeItem(key);
        } else {
            Backbone.Model.prototype.unset.call(this, key);
        }
        return this;
    },

    clear: function() {
        if(this.supportStorage) {
            this.storage.clear();
        } else {
            Backbone.Model.prototype.clear(this);
        }
    },

    setAsRegistered: function(participant_id, token) {
        this.set('registered', true);
        this.set('token', token);
        if (App.participant == undefined) {
            App.participant = new App.Models.Participant({participant_id: participant_id});
        }
    },

    token: function() {
        return this.get('token');
    },

    ping: function(token) {
        // Check to see if Participant exists, and is not completed
        // TODO Rewrite Method for API
        $.ajax({
            url: App.Settings.Api.url + apiUrl('ping', token),
            type: 'GET'
        }).done(function(response) {
            if (response.message == "true") {
                return true;
            } else if (response.message == "false") {
                return false;
            } else {
                return false;
            }
        });
    },

    isRegistered: function() {
        if (this.get('registered')) {
            if (App.participant == undefined) {
                App.participant = new App.Models.Participant();
                // Retrieve Saved User Details
                // TODO Write Participant Method to Retrieve Saved User Details
            }
            return true;
        } else if (getCookie('token') != null) {
            var token = getCookie('token');
            if (this.ping(token)) {
                this.supportStorage = false;
                this.set('token', token);
                App.participant = new App.Models.Participant();
                // Run Participant Method to Retrieve Saved Details
                this.setAsRegistered(App.participant.get('participant_id'), token);
            } else {
                App.router.navigate('/', {trigger: true});
            }
        }
    },

    register: function(credentials) {
        var scope = this;
        var success = function(model, response, options) {
            if (response.error) {
                // Handle Error
                // TODO Handle Registration Error
            } else {
                scope.setAsRegistered(response.participation_id, response.access_token);
                App.router.navigate('select-experiment', {trigger: true});
            }
        };
        var error = function(model, response, options) {
            // TODO Handle Non 200 Response
        };
        App.participant = new App.Models.Participant(credentials);
        App.participant.save(null, {success: success, error: error});
    },

    logout: function(callback) {
        var scope = this;
        // TODO Close Experiment & Clear Participant Information
    }
});
