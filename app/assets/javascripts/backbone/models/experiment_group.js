App.Models.ExperimentGroup = Backbone.Model.extend({
    initialize: function() {
        this.productCategories = new App.Collections.ExperimentCategories();
        if (this.get('experiment_group_categories')) {
            this.productCategories.set(this.get('experiment_group_categories'));
        }
        this.on('change:experiment_group_categories', this.updateCategories);
    },

    updateCategories: function() {
        this.productCategories.reset(this.get('experiment_group_categories'));
    },

    postSetupHook: function() {
        this.productCategories.postSetupHook();
    },

    getProductCategories: function() {
        return this.productCategories;
    }
});
