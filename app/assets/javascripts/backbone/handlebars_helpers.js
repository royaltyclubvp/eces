Handlebars.registerHelper('collectionSelectList', function(collection) {
    this.output = '';
    collection.each(function(model) {
        this.output = this.output + '<option value="' + model.get('id') + '">' + model.get('name') + '</option>';
    }, this);
    return this.output;
});