App.init = function() {
    // Pre-Load Experiment Data
    App.collections.experiments = new App.Collections.Experiments(null);
    App.collections.experiments.fetch();

    // Initialize Session
    App.session = new App.Models.Session();

    // Instantiate View Controller
    App.views.viewController = new App.Views.ViewController();

    // Instantiate Router & Start History
    App.router = new App.Router;

    // Globally Capture Clicks and Pass to Router for Push State Navigation
    $(document).on('click', "a[href^='/']", function(e) {
        var href = $(e.currentTarget).attr('href');

        if (!e.altKey && !e.ctrlKey && !e.metaKey && !e.shiftKey) {
            e.preventDefault();

            var url = href.replace(/^\//,'').replace('\#\!\/','');

            App.router.navigate(url, {trigger: true});

            return false;
        }
    });

    Backbone.history.start({pushState: true});
};
