App.Controllers.Registration = {
    SignUp: function() {
        Backbone.trigger('registration');
        App.views.viewController.showContentView(new App.Views.ParticipantStart(), 'participant-start');
    },
    
    SelectExperiment: function() {
        Backbone.trigger('registration');
        App.views.viewController.showContentView(new App.Views.ExperimentSelection(), 'experiment-selection')
    }
};