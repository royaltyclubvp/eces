App.Controllers.Experiment = {
    Home: function() {
        Backbone.trigger('experiment');
        App.views.viewController.showContentView(new App.Views.Home(), 'experiment-home', false);
    },
    ProductCategory: function(category_slug) {
        Backbone.trigger('experiment');
        var category = App.participant.experimentGroup.productCategories.findWhere({slug: category_slug});
        App.views.viewController.showContentView(new App.Views.ProductCategory({collection: category.products}), 'product-category', category_slug);
        App.views.viewController.postRenderHook();
    },
    ProductDetail: function(category_slug, product_slug) {
        Backbone.trigger('experiment');
        var product = App.participant.experimentGroup.productCategories.findWhere({slug: category_slug}).products.findWhere({slug: product_slug});
        App.views.viewController.showContentView(new App.Views.ProductDetail({model: product}), 'product-detail', category_slug);
    },
    ShoppingCart: function() {
        Backbone.trigger('experiment');
        App.views.viewController.showContentView(new App.Views.ShoppingCart({collection: App.participant.shoppingCart}), 'shopping-cart', 'shopping-cart');
    },
    Completed: function() {
        Backbone.trigger('experiment');
        App.views.viewController.showContentView(new App.Views.Completed(), 'completed', 'completed');
    }
};
