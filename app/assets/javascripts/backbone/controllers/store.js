var app = app || {};

app.StoreRouter = Backbone.Router.extend({

    routes: {
        "home": "showHome",
        "category/:id": "showCategory",
        "product/:id": "showProduct",
        "shopping-cart": "showCart",
        "checkout": "checkout"
    },

    initialize: function(application) {
        this.application = application;
    },

    showHome: function() {
        this.application.appView.showView(this.application.homeView);
    },

    showCategory: function(id) {
        if (this.application.experiment_group.get('step_by_step')) {
            if ((typeof this.application.previous_category != 'undefined') && id == (this.application.previous_category))  {
                this.loadCategory(id);
            } else if (this.application.move_to_next_category) {
                if (this.application.next_category == id) {
                    this.loadCategory(id);
                } else {
                    this.application.router.navigate('category/' + this.application.category_index - 1);
                    alert(this.application.experiment_group.get('step_by_step_inappropriate_category_message'));
                }
            } else {
                this.application.router.navigate('category/' + this.application.category_index - 1);
                alert(this.application.experiment_group.get('step_by_step_no_selection_message'));
            }
        } else {
            this.loadCategory(id);
        }
    },

    loadCategory: function(id) {
        var self = this;
        var searchArrayResult = $.inArray(id, self.application.loadedCategoriesDirectory);
        var collection;
        if (searchArrayResult >= 0) {
            this.categoryView = this.application.loadedCategories[searchArrayResult];
            this.application.appView.reShowView(this.categoryView);
        } else {
            this.productCategory = new app.ProductCategory();
            this.categoryView = new app.CategoryView({
                collection: this.productCategory,
                application: this.application,
                priceposition: this.application.experiment_group.get('price_location'),
                addtocartposition: this.application.experiment_group.get('add_to_basket_location')
            });
            this.productCategory.fetch({
                traditional: true,
                reset: true,
                data: {category: id, randomization: this.application.experiment_group.get('randomization_of_products')}
            });
            this.application.loadedCategoriesDirectory.push(id);
            this.application.loadedCategories.push(this.categoryView);
            this.application.previous_category = this.application.category_ids[this.application.category_index];
            this.application.category_index = this.application.category_index + 1;
            this.application.next_category = this.application.category_ids[this.application.category_index];
            this.application.move_to_next_category = false;
            this.application.appView.showViewSaveOld(this.categoryView);
        }
        this.categoryView.afterRender(this.categoryView);
    },

    showProduct: function(id) {
        var product = this.productCategory.get(id);
        this.productDetailView = new app.ProductDetailView({model: product, application: this.application, priceposition: this.application.experiment_group.get('price_location'), addtocartposition: this.application.experiment_group.get('add_to_basket_location')});
        this.application.appView.showView(this.productDetailView);
    },

    showCart: function() {
        this.shoppingCart = this.application.shoppingCart;
        this.shoppingCartView = new app.ShoppingCartView({collection: this.shoppingCart, application: this.application});
        this.application.appView.showView(this.shoppingCartView);
    },

    checkout: function() {
        this.experimentEndView = new app.ExperimentEndView({application: this.application, model: this.application.experiment});
        this.application.appView.showView(this.experimentEndView);
    }

});