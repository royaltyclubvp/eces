App.Router = Backbone.Router.extend({
    routes: {
        '': App.Controllers.Registration.SignUp,
        'select-experiment': App.Controllers.Registration.SelectExperiment,
        'home': App.Controllers.Experiment.Home,
        'cart': App.Controllers.Experiment.ShoppingCart,
        'completed': App.Controllers.Experiment.Completed,
        ':category/:product': App.Controllers.Experiment.ProductDetail,
        ':category': App.Controllers.Experiment.ProductCategory
    },

    authenticate: function(callback) {
        if (App.session.isRegistered()) {
            callback();
        } else {
            this.navigate('', {trigger: true});
        }
    }
});
