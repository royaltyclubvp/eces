App.Views.MainMenuItem = Backbone.View.extend({
    tagName: 'li',

    template: HandlebarsTemplates['menus/main_menu_item'],

    events: {
        'click a': 'openCategory'
    },

    initialize: function() {
        this.listenTo(this.model, 'change', this.render);
        this.listenTo(Backbone, 'page:' + this.model.get('slug'), this.highlightItem);
        this.render();
    },

    render: function() {
        this.$el.empty();
        this.$el.html(this.template(this.model.toJSON()));
    },

    openCategory: function() {
        var clickDetail;
        if (App.participant.experimentGroup.get('step_by_step')) {
            var next = true;
            var notification;
            var currentCategory = App.participant.experimentGroup.getProductCategories().currentCategory();
            if (currentCategory && !currentCategory.checkIfProductAdded()) {
                next = false;
                notification = App.participant.experimentGroup.get('step_by_step_no_selection_message');
                clickDetail = 'Rejected:No Items Selected In Current Category';
            }
            if (next && currentCategory && App.participant.experimentGroup.getProductCategories().nextCategory() != this.model) {
                next = false;
                notification = App.participant.experimentGroup.get('step_by_step_inappropriate_category_message');
                clickDetail = 'Rejected:Category Selected Was Not Next In Order';
            }
            if (next && !currentCategory && App.participant.experimentGroup.getProductCategories().nextCategory() != this.model) {
                next = false;
                notification = App.participant.experimentGroup.get('inappropriate_start_point_message');
                clickDetail = "Rejected:User Attempted To Start From Wrong Category";
            }
            if (next) {
                App.participant.experimentGroup.getProductCategories().moveToNext();
                clickDetail = 'Accepted';
                App.router.navigate(this.model.get('slug'), {trigger: true});
            } else {
                Backbone.trigger('notification', notification);
            }
        } else {
            clickDetail = 'Accepted';
            App.router.navigate(this.model.get('slug'), {trigger: true});
        }
        this.recordClick(clickDetail);
    },

    recordClick: function(details) {
        App.participant.experimentClicks.create({click_type: 'MenuCategory:' + this.model.get('name') + ':' + details});
    },

    highlightItem: function() {
        this.$el.addClass('active');
    }
});
