App.Views.MainMenu = Backbone.View.extend({
    initialize: function(parentScope, menuId) {
        this.listenTo(Backbone, 'menu:newItem', this.highlightNewSelection);
        this.$el = parentScope.$('#' + menuId);
        this.childViews = [];
        this.render();
    },

    render: function() {
        this.$el.empty();
        var container = document.createDocumentFragment();
        // Cycle through Experiment Group Categories and Add View for Each.
        App.participant.experimentGroup.productCategories.each(function(category) {
            var view = new App.Views.MainMenuItem({model: category});
            container.appendChild(view.el);
            this.childViews.push(view);
        }, this);
        this.$el.html(container);
    },

    highlightNewSelection: function(page) {
        this.$('.active').removeClass('active');
        Backbone.trigger('page:' + page);
    }
});
