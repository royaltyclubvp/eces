App.Views.Registration = Backbone.View.extend({
    id: 'inner-container',
    
    template: HandlebarsTemplates['app-frames/registration'],
    
    initialize: function() {
        this.listenTo(Backbone, 'registration:loading', this.showLoadingIndicator);
        this.listenTo(Backbone, 'registration:loaded', this.hideLoadingIndicator);
        this.render();
    },
    
    render: function() {
        this.$el.empty();
        this.$el.html(this.template());
        this.$contentContainer = this.$('#content-container');
        this.$loadingContainer = this.$('#loading-container');
    },
    
    renderSubView: function(view) {
        this.$contentContainer.html(view.$el);
    },
    
    showLoadingIndicator: function() {
        var scope = this;
        this.$contentContainer.hide('fast', function() {
            scope.$loadingContainer.show('fast');
        });
    },
    
    hideLoadingIndicator: function() {
        var scope = this;
        this.$loadingContainer.hide('fast', function() {
            scope.$contentContainer.show('fast');
        });
    }
});