App.Views.Experiment = Backbone.View.extend({
    id: 'inner-container',

    template: HandlebarsTemplates['app-frames/experiment'],

    events: {
        'click .cart-icon': 'openShoppingCart'
    },

    initialize: function() {
        this.childViews = [];
        this.render();
    },

    render: function() {
        this.$el.empty();
        this.setBackground();
        this.setBodyTextColor();
        this.$el.html(this.template({
            logo_filename: App.participant.experimentGroup.get('logo_filename').url,
            slogan: App.participant.experimentGroup.get('slogan')
        }));
        // Load Main Menu
        this.mainMenu = new App.Views.MainMenu(this, 'menu-container');
        this.childViews.push(this.mainMenu);
        this.$contentContainer = this.$('#content-container');
        this.$leftCartIcon = this.$('#cart-left');
        this.$rightCartIcon = this.$('#cart-right');
        this.setShoppingCartIcon();
    },

    renderSubView: function(view) {
        this.$contentContainer.html(view.$el);
    },

    setBackground: function() {
        var backgroundType = App.participant.experimentGroup.get('background_type');
        if (backgroundType == 'Image') {
            Backbone.trigger('background_image:change', App.participant.experimentGroup.get('background_image').url);
        } else if (backgroundType == 'Color') {
            Backbone.trigger('background_color:change', App.participant.experimentGroup.get('background_color'));
        }
    },

    setBodyTextColor: function() {
        if (App.participant.experimentGroup.get('font_color')) {
            Backbone.trigger('body_text_color:change', App.participant.experimentGroup.get('font_color'));
        }
    },

    setShoppingCartIcon: function() {
        if (App.participant.experimentGroup.get('cart_position') == 'topleft') {
            this.$leftCartIcon.removeClass('invisible');
        } else if (App.participant.experimentGroup.get('cart_position') == 'topright') {
            this.$rightCartIcon.removeClass('invisible');
        }
    },

    openShoppingCart: function() {
        App.participant.experimentClicks.create({click_type: 'View Cart'});
        App.router.navigate('cart', {trigger: true});
    },

});
