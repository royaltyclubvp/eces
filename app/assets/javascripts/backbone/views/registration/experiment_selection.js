App.Views.ExperimentSelection = Backbone.View.extend({
    className: 'row',

    template: HandlebarsTemplates['registration/experiment_selection'],

    events: {
        'click #select-experiment': 'selectExperiment'
    },

    initialize: function() {
        this.render();
    },

    render: function() {
        this.$el.html(this.template({participation_id: App.participant.get('participation_id'), experiments: App.collections.experiments}));
        this.$experimentSelection = this.$('#experiment');
    },

    selectExperiment: function() {
        var experiment = this.$experimentSelection.val();
        Backbone.trigger('registration:loading');
        // Listen for Completion of Group Assignment
        this.listenTo(App.participant, 'experiment_group_assigned', this.groupAssigned);
        this.listenTo(App.participant, 'experiment_group_assignment_failed', this.groupAssignmentFailure);
        App.participant.selectExperiment(experiment);
    },

    groupAssigned: function() {
        this.stopListening(App.participant, 'experiment_group_assigned experiment_group_assignment_failed');
        Backbone.trigger('registration:loaded');
        // Listen for Start Confirmation & Load Confirmation Model
        this.listenTo(Backbone, 'confirmation_response', this.handleStartConfirmation);
        Backbone.trigger('confirmation', App.Dictionary.Registration.confirmStart);
    },

    groupAssignmentFailure: function() {
        this.stopListening(App.participant, 'experiment_group_assigned experiment_group_assignment_failed');
        Backbone.trigger('registration:loaded');
    },

    handleStartConfirmation: function(response) {
        if (response) {
            // Set Experiment Start Time
            App.participant.experiment.setStartTime(Date.now());
            App.router.navigate('home', {trigger: true});
        } else {
            App.router.navigate('', {trigger: true});
        }
    }
});
