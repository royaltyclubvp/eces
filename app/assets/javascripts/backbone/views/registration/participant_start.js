App.Views.ParticipantStart = Backbone.View.extend({
    className: 'row',

    template: HandlebarsTemplates['registration/participant_start'],

    events: {
        'click #register': 'register'
    },

    initialize: function() {
        this.render();
    },

    render: function() {
        this.$el.html(this.template());
        this.$participationId = this.$('#participation-id');
    },

    register: function() {
        var participationId = this.$participationId.val();
        if (participationId != undefined && participationId != '') {
            App.session.register({participation_id: participationId});
        } else {
            Backbone.trigger('notification', App.Dictionary.Registration.emptyParticipationIdField);
        }
    }
});