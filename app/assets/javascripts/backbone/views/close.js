Backbone.View.prototype.close = function() {
    this.remove();
    this.unbind();
    if (this.onClose) {
        this.onClose();
    }
    if (this.childViews) {
        for (var i = 0; this.childViews[i]; i++) {
            this.childViews[i].close();
        }
    }
};