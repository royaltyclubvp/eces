App.Views.ConfirmationModal = Backbone.View.extend({
    id: 'confirmation-container',
    className: 'text-center center-block',

    template: HandlebarsTemplates['modals/confirmation_modal'],

    events: {
        'click .btn-yes': 'confirm',
        'click .btn-no': 'reject'
    },

    initialize: function(options) {
        this.listenTo(Backbone, 'confirmation', this.render);
    },

    render: function(message) {
        this.$el.empty();
        this.$el.html(this.template({message: message}));
        Backbone.trigger('confirmation:show');
    },

    close: function() {
        Backbone.trigger('confirmation:hide');
    },

    confirm: function() {
        this.close();
        Backbone.trigger('confirmation_response', true);
    },

    reject: function() {
        this.close();
        Backbone.trigger('confirmation_response', false);
    }

});