App.Views.NotificationModal = Backbone.View.extend({
    id: 'notification-container',
    className: 'text-center center-block',

    template: HandlebarsTemplates['modals/notification_modal'],

    events: {
        'click button': 'close'
    },

    initialize: function(options) {
        this.listenTo(Backbone, 'notification', this.render);
    },

    render: function(message) {
        this.$el.empty();
        this.$el.html(this.template({message: message}));
        Backbone.trigger('notification:show');
    },

    close: function() {
        Backbone.trigger('notification:hide');
    }
});
