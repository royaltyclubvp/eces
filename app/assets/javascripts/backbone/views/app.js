App.Views.ViewController = Backbone.View.extend({
    el: 'body',

    // App Frame
    $appFrame: $('#app-frame'),

    // Modal Frames
    $notificationModalFrame: $('#notification-modal'),
    $confirmationModalFrame: $('#confirmation-modal'),

    initialize: function() {
        // Set App Frame Views
        this.registrationView = App.Views.Registration;
        this.experimentView = App.Views.Experiment;

        // Initiate Class Variables
        this.previousClasses = '';
        // Initialize Notification & Confirmation Modals
        this.notificationModal = new App.Views.NotificationModal();
        this.$notificationModalFrame.html(this.notificationModal.el);
        this.confirmationModal = new App.Views.ConfirmationModal();
        this.$confirmationModalFrame.html(this.confirmationModal.el);
        this.listenTo(Backbone, 'registration',this.switchToRegistration);
        this.listenTo(Backbone, 'experiment', this.switchToExperiment);
        this.listenTo(Backbone, 'background_color:change', this.setBackgroundColor);
        this.listenTo(Backbone, 'background_image:change', this.setBackgroundImage);
        this.listenTo(Backbone, 'body_text_color:change', this.setBodyTextColor);
        this.listenTo(Backbone, 'notification:show', this.showNotificationModal);
        this.listenTo(Backbone, 'notification:hide', this.hideNotificationModal);
        this.listenTo(Backbone, 'confirmation:show', this.showConfirmationModal);
        this.listenTo(Backbone, 'confirmation:hide', this.hideConfirmationModal);
    },

    render: function() {
        this.$appFrame.empty();
        this.$appFrame.html(this.view.$el);
    },

    // Show Content View
    // view: Backbone view object
    // classes: Classes to apply to App Frame
    // page: New page to highlight on menu. Pass false if not applicable
    showContentView: function(view, classes, page) {
        if (this.currentView) {
            this.currentView.close();
        }
        if (page != false) {
            Backbone.trigger('menu:newItem', page);
        }
        this.currentView = view;
        this.$appFrame.removeClass(this.previousClasses);
        this.previousClasses = classes;
        this.$appFrame.addClass(classes);
        this.view.renderSubView(view);
    },

    contentView: function() {
        return this.currentView;
    },

    // Run Post Render Hook on Current View
    postRenderHook: function() {
        this.currentView.postRenderHook();
    },

    // Switch to Registration App Frame
    switchToRegistration: function() {
        if (this.viewType != 'registration') {
            if (this.view) {
                this.view.close();
            }
            this.view = new this.registrationView();
            this.viewType = 'registration';
            this.$appFrame.removeClass('experiment');
            this.$appFrame.addClass('registration');
            this.render();
        }
    },

    // Switch to Experiment App Frame
    switchToExperiment: function() {
        if (this.viewType != 'experiment') {
            if (this.view) {
                this.view.close();
            }
            this.view = new this.experimentView();
            this.viewType = 'experiment';
            this.$appFrame.removeClass('registration');
            this.$appFrame.addClass('experiment');
            this.render();
        }
    },

    setBackgroundColor: function(color) {
        this.$el.css('background-color', color);
    },

    setBackgroundImage: function(image) {
        this.$el.css({'background-image': 'url(' + image + ')', 'background-size': 'cover'});
        this.$el.addClass('background');
    },

    setBodyTextColor: function(color) {
        this.$el.css('color', color);
    },

    // Show Notification Modal
    showNotificationModal: function() {
        this.$appFrame.addClass('modal-bg');
        this.$notificationModalFrame.show('fast');
    },

    // Hide Notification Modal
    hideNotificationModal: function() {
        this.$notificationModalFrame.hide('fast');
        this.$appFrame.removeClass('modal-bg');
    },

    // Show Confirmation Modal
    showConfirmationModal: function() {
        this.$appFrame.addClass('modal-bg');
        this.$confirmationModalFrame.show('fast');
    },

    // Hide Confirmation Modal
    hideConfirmationModal: function() {
        this.$confirmationModalFrame.hide('fast');
        this.$appFrame.removeClass('modal-bg');
    }

});
