var app = app || {};

app.ExperimentEndView = Backbone.View.extend ({

    template: '',

    initialize: function(options) {
        this.model = options.model;
        this.application = options.application;
        if(this.application.experiment_group.get('show_survey_link') == true) {
            this.template = $('#checkout-with-survey-link-template').html();
        } else {
            this.template = $('#checkout-without-survey-link-template').html();
        }
        Mustache.parse(this.template)
    },

    render: function() {
        this.$el.html(Mustache.render(this.template, this.model));
        return this;
    }

});