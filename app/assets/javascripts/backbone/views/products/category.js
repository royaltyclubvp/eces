var app = app || {};

app.CategoryView = Backbone.View.extend ({

    className: "center-block",

    id: 'products',

    initialize: function(options) {
        this.childViews = [];
        this.collection = options.collection;
        this.application = options.application;
        this.priceposition = options.priceposition;
        this.addtocartposition = options.addtocartposition;
        if (this.application.experiment_group.get('out_of_stock') == true) {
            if (this.application.experiment_group.get('out_of_stock_per_category') == true) {
               this.out_of_stock_per_category_flag = true;
            } else {
                if (typeof this.application.out_of_stock_flag == 'undefined') {
                    this.application.out_of_stock_flag = true;
                }
            }
        }
        this.category_selection_limit = this.application.experiment_group.get('category_selection_limit');
        if (this.category_selection_limit != 0) {
            this.application.category_limit = true;
            this.category_counter = 0;
            this.application.category_selection_limit = this.category_selection_limit;
        }
        this.listenTo(this.collection, 'reset', this.render);
        this.viewedProducts = [];
    },

    render: function() {
        this.addAll();
        return this;
    },


    afterRender: function(scope) {
        if (scope.childViews.length > 0) {
            if (scope.intervalId) {
                clearInterval(scope.intervalId);
            }
            for (var i = 0, len = scope.childViews.length; i < len; i++) {
                scope.childViews[i].afterRender(scope);
            }
        } else {
            if (!scope.intervalId) {
                scope.intervalId = setInterval(function() {
                    scope.afterRender(scope);
                }, 1000);
            }
        }
    },

    addViewedProduct: function(id) {
        if ($.inArray(id, this.viewedProducts) == -1) {
            this.viewedProducts.push(id);
            var productView = new app.ExperimentView();
            productView.set('product_id', id);
            productView.save();
        }
    },

    addOne: function(product) {
        var view = new app.ProductView({model: product, application: this.application, parentView: this});
        view.model.trigger("model_loaded");
        view.delegateEvents();
        this.childViews.push(view);
        this.$el.append(view.render().el);
    },

    addAll: function() {
        this.$el.html(' ');
        this.collection.each(this.addOne, this);
        this.$el.append("<div class='clearfix'></div>");
        if(this.addtocartposition == 'aboveleft') {
            this.$el.addClass("add-to-cart-top-pull-left")
        } else if(this.addtocartposition == 'aboveright') {
            this.$el.addClass("add-to-cart-top-pull-right")
        } else if(this.addtocartposition == 'belowleft') {
            this.$el.addClass("add-to-cart-bottom-pull-left")
        } else if(this.addtocartposition == 'belowright') {
            this.$el.addClass("add-to-cart-bottom-pull-right")
        }
        if(this.priceposition == 'aboveleft') {
            this.$el.addClass("price-top-pull-left")
        } else if(this.priceposition == 'aboveright') {
            this.$el.addClass("price-top-pull-right")
        } else if(this.priceposition == 'belowleft') {
            this.$el.addClass("price-bottom-pull-left")
        } else if(this.priceposition == 'belowright') {
            this.$el.addClass("price-bottom-pull-right")
        }
    }
});