var app = app || {};

app.HomeView = Backbone.View.extend ({

    template: $('#home-template').html(),

    initialize: function() {
        this.listenTo(this.model, 'change', this.render);
        Mustache.parse(this.template)
    },

    render: function() {
        this.$el.html(Mustache.render(this.template, this.model));
        return this;
    }

});