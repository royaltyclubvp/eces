app = app || {};

app.ProductDetailView = Backbone.View.extend({

    template: $('#product-detail-template').html(),

    events: {
        'click .add-to-cart': 'addToCart',
        'click .remove-from-cart': 'removeFromCart',
        'click #back-to-category': 'registerBackToCategory',
        'click #back-to-home': 'registerBackToHome'
    },

    initialize: function(options) {
        this.model = options.model;
        this.application = options.application;
        this.priceposition = options.priceposition;
        this.addtocartposition = options.addtocartposition;
        this.listenTo(this.model, 'change', this.render);
        Mustache.parse(this.template);
    },

    render: function() {
        this.viewingTimer = new App.Models.ExperimentProductDetailView({product_id: this.model.get('id')});
        App.participant.experimentProductDetailViews.add(this.viewingTimer);
        this.$el.html(Mustache.render(this.template, this.model));
        if(this.addtocartposition == 'aboveleft') {
            this.$el.addClass("add-to-cart-top-pull-left")
        } else if(this.addtocartposition == 'aboveright') {
            this.$el.addClass("add-to-cart-top-pull-right")
        } else if(this.addtocartposition == 'belowleft') {
            this.$el.addClass("add-to-cart-bottom-pull-left")
        } else if(this.addtocartposition == 'belowright') {
            this.$el.addClass("add-to-cart-bottom-pull-right")
        }
        if(this.priceposition == 'aboveleft') {
            this.$el.addClass("price-top-pull-left")
        } else if(this.priceposition == 'aboveright') {
            this.$el.addClass("price-top-pull-right")
        } else if(this.priceposition == 'belowleft') {
            this.$el.addClass("price-bottom-pull-left")
        } else if(this.priceposition == 'belowright') {
            this.$el.addClass("price-bottom-pull-right")
        }
        if (this.model.get('in_cart') == 'true') {
            this.$('.add-to-cart').html("Remove from Cart");
            this.$('.add-to-cart').addClass('remove-from-cart');
            this.$('.remove-from-cart').removeClass('add-to-cart');
        }
        return this;
    },

    addToCart: function() {
        this.application.shoppingCart.add(this.model);
        this.model.toggleInCart();
        var clicktype = "Add To Cart. Product: " + this.model.get('title');
        this.registerClick(clicktype);
        alert("The item has been successfully added to your cart");
        this.$('.add-to-cart').html("Remove from Cart");
        this.$('.add-to-cart').addClass('remove-from-cart');
        this.$('.remove-from-cart').removeClass('add-to-cart');
    },

    removeFromCart: function() {
        this.application.shoppingCart.remove(this.model);
        this.model.toggleOutCart();
        var clicktype = "Remove From Cart. Product: " + this.model.get('title');
        this.registerClick(clicktype);
        alert("The item has been successfully remove from your cart");
        this.$('.remove-from-cart').html("Add to Cart");
        this.$('.remove-from-cart').addClass('add-to-cart');
        this.$('.add-to-cart').removeClass('remove-from-cart');
    },

    registerBackToCategory: function() {
        this.registerClick("Back To Category");
    },

    registerBackToHome: function() {
        this.registerClick("Back To Home");
    },

    registerClick: function(clicktype) {
        App.participant.experimentClicks.create({click_type: clicktype});
    },

    onClose: function() {
        this.viewingTimer.calculateViewTime().save();
    }
});
