App.Views.ProductCartLine = Backbone.View.extend({
    tagName: 'tr',

    template: HandlebarsTemplates['products/product_cart_line'],

    events: {
        'click .remove-from-cart': 'removeFromCart'
    },

    initialize: function() {
        this.render();
    },

    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
    },

    removeFromCart: function() {
        var detail = 'Remove From Cart(Shopping Cart Page):' + this.model.get('title');
        if (App.participant.shoppingCart.checkIfItemRemoved()) {
            detail += '(Forced to Remove)';
        }
        this.recordRemovalClick(detail);
        App.participant.shoppingCart.remove(this.model);
        App.participant.shoppingCart.confirmItemRemoved();
    },

    recordRemovalClick: function(detail) {
        App.participant.experimentClicks.create({click_type: detail});
    }
});
