App.Views.ProductPreview = Backbone.View.extend(
    _.extend({}, App.Mixins.ProductOptions, {
    className: 'col-xs-3 product',

    template: HandlebarsTemplates['products/product_preview'],

    events: {
        'click .add-to-basket-button': 'addToBasket',
        'click .remove-from-basket-button': 'removeFromBasket',
        'click .product-title': 'recordTitleClick'
    },

    initialize: function() {
        this.listenTo(this.model, 'change', this.render);
        this.render();
    },

    render: function() {
        this.$el.empty();
        var jsonWithDetailOption = {detail_page: App.participant.experimentGroup.get('detail_page')};
        $.extend(jsonWithDetailOption, this.model.toJSON());
        this.$el.html(this.template(jsonWithDetailOption));
        this.setPriceLocation();
        this.setCartButtonLocation();
    },

    postRenderHook: function() {
        if (this.$el.visible()) {
            this.model.category.addToViewed(this.model.get('id'));
        } else {
            var scope = this;
            this.waypoint = this.$el.waypoint({
                handler: function() {
                    scope.model.category.addToViewed(scope.model.get('id'));
                },
                offset: 'bottom-in-view'
            });
        }
    },

    recordTitleClick: function() {
        App.participant.experimentClicks.create({click_type: 'Open Detail Page: ' + this.model.get('title')});
        // Record Product Select
        App.participant.experimentSelects.create({product_id: this.model.get('id')});
    }
}));
