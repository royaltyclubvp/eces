app = app || {};

app.ProductCartItemView = Backbone.View.extend({
    tagName: 'tr',

    className: 'product',

    template: $('#shopping-cart-item-template').html(),

    events: {
        'click .remove-item-from-cart': 'removeFromCart'
    },

    initialize: function(options) {
        this.listenTo(this.model, 'change', this.render);
        this.collection = options.collection;
        this.application = options.application;
        Mustache.parse(this.template);
    },

    render: function() {
        this.$el.html(Mustache.render(this.template, this.model));
        return this;
    },

    removeFromCart: function() {
        this.registerItemRemovalClick();
        if (this.application.forceCartActivated) {
            this.application.itemRemoved = true;
        }
        this.collection.remove(this.model);
    },

    registerItemRemovalClick: function() {
        var itemName = this.model.get('title');
        this.registerClick("Remove Item From Cart: " + itemName);
    },

    registerClick: function(clicktype) {
        App.participant.ExperimentClicks.create({click_type: clicktype});
        //this.application.appView.registerClick(clicktype);
    }
});
