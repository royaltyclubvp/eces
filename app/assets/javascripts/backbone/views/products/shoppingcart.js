var app = app || {};

app.ShoppingCartView = Backbone.View.extend({
    className: "center-block",

    id: "shopping-cart-page",

    template: $('#shopping-cart-template').html(),

    events: {
        'click #checkout-button': 'registerCheckOutClick'
    },

    initialize: function(options) {
        this.listenTo(options.collection, 'reset', this.render);
        this.listenTo(options.collection, 'remove', this.render);
        this.collection = options.collection;
        this.application = options.application;
        if (this.application.experiment_group.get('force_cart_removal') == true) {
            this.forceCartRemoval = true;
            this.application.itemRemoved = false;
            this.application.forceCartActivated = false;
        } else {
            this.forceCartRemoval = false;
        }
        Mustache.parse(this.template);
    },

    render: function() {
        this.addAll();
        return this;
    },

    addOneAscending: function(product) {
        var view = new app.ProductCartItemView({model: product, collection: this.collection, application: this.application});
        view.model.trigger("model_loaded");
        this.$('tbody').append(view.render().el);
    },

    addOneDescending: function(product) {
        var view = new app.ProductCartItemView({model: product, collection: this.collection, application: this.application});
        view.model.trigger("model_loaded");
        this.$('tbody').prepend(view.render().el);
    },

    addAll: function() {
        this.$el.html(' ');
        this.$el.html(Mustache.render(this.template));
        if(this.application.experiment_group.get('basket_order') == 'Ascending') {
            this.collection.each(this.addOneAscending, this);
        } else if(this.application.experiment_group.get('basket_order') == 'Descending') {
            this.collection.each(this.addOneDescending,this);
        }
        this.$el.append("<div class='clearfix'></div>");
    },

    registerCheckOutClick: function(event) {
        if (this.forceCartRemoval) {
            if(this.application.itemRemoved) {
                var endTime = new Date().getTime() / 1000;
                var completionTime = endTime - this.application.startTime;
                var result = new app.ExperimentResult();
                result.set('completion_time', completionTime);
                result.save();
                _(this.collection.models).each(function(item) {
                    $.ajax({
                        url: "/purchases",
                        method: "POST",
                        data: {product_id: item.get('id')}
                    });
                });
                this.registerClick("Checkout");
            } else {
                this.application.forceCartActivated = true;
                event.preventDefault();
                alert(this.application.experiment_group('force_cart_removal_message'));
                this.registerClick("Checkout Attempt");
            }
        }  else {
            var endTime = new Date().getTime() / 1000;
            var completionTime = endTime - this.application.startTime;
            var result = new app.ExperimentResult();
            result.set('completion_time', completionTime);
            result.save();
            _(this.collection.models).each(function(item) {
                $.ajax({
                    url: "/purchases",
                    method: "POST",
                    data: {product_id: item.get('id')}
                });
            });
            this.registerClick("Checkout");
        }
    },

    registerClick: function(clicktype) {
        App.participant.experimentClicks.create({click_type: clicktype});
        //this.application.appView.registerClick(clicktype);
    }
});
