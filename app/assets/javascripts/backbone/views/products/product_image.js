App.Views.ProductImage = Backbone.View.extend({
    className: 'col-xs-6',

    template: HandlebarsTemplates['products/product_image'],

    initialize: function() {
        this.render();
    },

    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
    }
});
