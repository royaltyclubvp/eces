App.Views.Home = Backbone.View.extend({
    id: 'inner-content-container',
    className: 'col-xs-12',

    template: HandlebarsTemplates['pages/home'],

    initialize: function() {
        this.listenTo(App.participant.experimentGroup, 'change', this.render);
        this.childViews = [];
        this.render();
    },

    render: function() {
        this.$el.empty();
        this.$el.html(this.template({
            front_image_1: App.participant.experimentGroup.get('front_image_1').url,
            front_image_2: App.participant.experimentGroup.get('front_image_2').url,
            front_image_3: App.participant.experimentGroup.get('front_image_3').url
        }));
        // Trigger Carousel Start

        // Load Side Menu
        this.mainMenu = new App.Views.MainMenu(this, 'home-side-menu');
        this.childViews.push(this.mainMenu);
    }
});
