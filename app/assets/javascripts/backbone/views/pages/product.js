App.Views.ProductDetail = Backbone.View.extend(
    _.extend({}, App.Mixins.ProductOptions, {
    id: 'inner-content-container',
    className: 'col-xs-12',

    template: HandlebarsTemplates['pages/product'],

    events: {
        'click .add-to-basket-button': 'addToBasket',
        'click .remove-from-basket-button': 'removeFromBasket',
        'click #back-to-category': 'recordBackToCategoryClick'
    },

    initialize: function() {
        this.listenTo(this.model, 'change', this.render);
        this.childViews = [];
        this.viewingTimer = new App.Models.ExperimentProductDetailView({product_id: this.model.get('id')});
        App.participant.experimentProductDetailViews.add(this.viewingTimer);
        this.render();
    },

    render: function() {
        this.$el.empty();
        this.$el.html(this.template(this.model.toJSON()));
        this.setPriceLocation();
        this.setCartButtonLocation();
        this.$imagesContainer = this.$('#product-images');
        var container = document.createDocumentFragment();
        if (this.model.images) {
            this.model.images.each(function(image) {
                var view = new App.Views.ProductImage({model: image});
                this.childViews.push(view);
                container.appendChild(view.el);
            }, this);
            this.$imagesContainer.html(container);
        }
    },

    recordBackToCategoryClick: function() {
        this.recordClick('Return to Category Page', this.model.get('category_slug'));
    },

    onClose: function() {
        this.viewingTimer.calculateViewTime().save();
    }
}));
