App.Views.Completed = Backbone.View.extend({
    id: 'inner-content-container',
    className: 'col-xs-12',

    template: HandlebarsTemplates['pages/completed'],

    initialize: function() {
        this.render();
    },

    render: function() {
        this.$el.html(this.template(App.participant.experiment.toJSON()));
    }
});
