App.Views.ShoppingCart = Backbone.View.extend({
    id: 'inner-content-container',
    className: 'col-xs-12',

    template: HandlebarsTemplates['pages/shopping_cart'],

    events: {
        'click #checkout': 'checkout'
    },

    initialize: function() {
        this.listenTo(this.collection, 'update', this.render);
        this.childViews = [];
        this.render();
    },

    render: function() {
        this.$el.empty();
        this.$el.html(this.template({total_price: this.collection.getTotalPrice()}));
        this.$itemsContainer = this.$('tbody');
        this.renderProducts();
    },

    renderProducts: function() {
        this.$itemsContainer.empty();
        var container = document.createDocumentFragment();
        if (App.participant.experimentGroup.get('basket_order') == 'Ascending') {
            this.collection.each(function(product) {
                var view = new App.Views.ProductCartLine({model: product});
                this.childViews.push(view);
                container.appendChild(view.el);
            }, this);
        } else if (App.participant.experimentGroup.get('basket_order') == 'Descending') {
            for (var i = this.collection.length - 1; i >= 0; i--) {
                var view = new App.Views.ProductCartLine({model: this.collection.models[i]});
                this.childViews.push(view);
                container.appendChild(view.el);
            }
        }
        this.$itemsContainer.html(container);
    },

    checkout: function() {
        var clickDetail;
        if (App.participant.experimentGroup.get('force_cart_removal')) {
            if (App.participant.shoppingCart.checkIfItemRemoved()) {
                if (!App.participant.shoppingCart.length) {
                    clickDetail = 'Rejected:Empty Shopping Cart';
                    this.recordCheckOutClick(clickDetail);
                    Backbone.trigger('notification', 'Your cart is empty. You must add items to your cart before attempting to check out.');
                } else {
                    clickDetail = 'Accepted';
                    var completionTime = Math.ceil((Date.now() - App.participant.experiment.getStartTime())/1000);
                    App.participant.experimentResult.set({completed: true, completion_time: completionTime});
                    App.router.navigate('completed', {trigger: true});
                    this.recordCheckOutClick(clickDetail);
                    App.participant.finalizeResults();
                }
            } else {
                clickDetail = 'Rejected:An Item Must Be Removed From Cart First';
                this.recordCheckOutClick(clickDetail);
                Backbone.trigger('notification', App.participant.experimentGroup.get('force_cart_removal_message'));
            }
        } else {
            clickDetail = 'Accepted';
            this.recordCheckOutClick(clickDetail);
            var completionTime = Math.ceil((Date.now() - App.participant.experiment.getStartTime()/1000));
            App.participant.experimentResult.set({completed: true, completion_time: completionTime});
            App.router.navigate('completed', {trigger: true});
            App.participant.finalizeResults();
        }
    },

    recordCheckOutClick: function(detail) {
        App.participant.experimentClicks.create({click_type: 'Checkout:' + detail});
    }
});
