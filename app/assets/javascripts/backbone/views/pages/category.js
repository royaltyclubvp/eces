App.Views.ProductCategory = Backbone.View.extend({
    id: 'inner-content-container',

    initialize: function() {
        this.childViews = [];
        this.viewedProducts = [];
        this.listenTo(this.collection, 'reset', this.render);
        // Check if Category Selection Limit is Set to Non-Zero or Non-Null Value. If So, Initialize Category Seleetion Limit to 0
        if (App.participant.experimentGroup.get('category_selection_limit')) {
            this.categorySelectionLimit = App.participant.experimentGroup.get('category_selection_limit');
            this.categorySelectionCounter = 0;
        }
        this.viewingTimer = new App.Models.ExperimentCategoryView({category_id: this.collection.category.get('id')});
        App.participant.experimentCategoryViews.add(this.viewingTimer);
        this.render();
    },

    render: function() {
        this.$el.empty();
        var container = document.createDocumentFragment();
        this.collection.each(function(product) {
            var view = new App.Views.ProductPreview({model: product});
            container.appendChild(view.el);
            this.childViews.push(view);
        }, this);
        this.$el.html(container);
    },

    postRenderHook: function() {
        if (this.childViews.length) {
            if (this.intervalId) {
                clearInterval(this.intervalId);
            }
            for (var i = 0, len = this.childViews.length; i < len; i++) {
                this.childViews[i].postRenderHook();
            }
        } else {
            if (!this.intervalId) {
                scope = this;
                this.intervalId = setInterval(function() {
                    scope.postRenderHook();
                }, 1000);
            }
        }
    },

    onClose: function() {
        this.viewingTimer.calculateViewTime().save();
    }
});
