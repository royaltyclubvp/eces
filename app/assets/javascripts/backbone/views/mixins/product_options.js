App.Mixins.ProductOptions = {
    addToBasket: function() {
        var notification;
        var next = true;
        var clickDetail;
        if (App.participant.experimentGroup.get('out_of_stock')) {
            if (App.participant.experimentGroup.get('out_of_stock_per_category')) {
                if (!this.model.category.checkOutOfStock()) {
                    notification = App.participant.experimentGroup.get('out_of_stock_message');
                    next = false;
                    this.model.category.disableOutOfStock();
                    this.model.set('in_stock', false);
                    clickDetail = 'Rejected:Out Of Stock';
                }
            } else {
                if (!App.participant.checkOutOfStock()) {
                    notification = App.participant.experimentGroup.get('out_of_stock_message');
                    next = false;
                    this.model.set('in_stock', false);
                    App.participant.disableOutOfStock();
                    clickDetail = 'Rejected:Out Of Stock';
                }
            }
        }
        if (next && App.participant.experimentGroup.get('category_selection_limit')) {
            if (!this.model.category.checkLimit()) {
                notification = App.participant.experimentGroup.get('category_limit_message');
                next = false;
                clickDetail = 'Rejected:Category Selection Limit Reached';
            }
        }
        if (next) {
            clickDetail = 'Accepted';
            App.participant.shoppingCart.add(this.model);
        } else {
            Backbone.trigger('notification', notification);
        }
        this.recordClick('Add To Cart', this.model.get('title') + ':' + clickDetail);
    },

    removeFromBasket: function() {
        this.recordClick('Remove From Cart', this.model.get('title'))
        App.participant.shoppingCart.remove(this.model);
    },

    setPriceLocation: function() {
        var priceLocation = App.participant.experimentGroup.get('price_location');
        if (priceLocation == 'aboveleft') {
            this.$('.top-caption .price.pull-left').removeClass('hidden');
        } else if (priceLocation == 'aboveright') {
            this.$('.top-caption .price.pull-right').removeClass('hidden');
        } else if (priceLocation == 'belowleft') {
            this.$('.bottom-caption .price.pull-left').removeClass('hidden');
        } else if (priceLocation == 'belowright') {
            this.$('.bottom-caption .price.pull-right').removeClass('hidden');
        }
    },

    setCartButtonLocation: function() {
        var cartLocation = App.participant.experimentGroup.get('add_to_basket_location');
        if (cartLocation == 'aboveleft') {
            this.$('.top-caption .basket.pull-left').removeClass('hidden');
        } else if (cartLocation == 'aboveright') {
            this.$('.top-caption .basket.pull-right').removeClass('hidden');
        } else if (cartLocation == 'belowleft') {
            this.$('.bottom-caption .basket.pull-left').removeClass('hidden');
        } else if (cartLocation == 'belowright') {
            this.$('.bottom-caption .basket.pull-right').removeClass('hidden');
        }
    },

    recordClick: function(type, detail) {
        App.participant.experimentClicks.create({click_type: type + ':' + detail});
    }
}
