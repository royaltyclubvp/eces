var URLs = {
    experiments: function() {
        return '/experiment';
    },
    select_experiment: function(access_token) {
        return '/participants/' + access_token + '/select-experiment';
    },
    category_products: function(id, access_token) {
        return '/product_categories/' + id + '/' + access_token;
    }
};

var apiUrl = function(type) {
    return URLs[type] ? URLs[type].apply(this, [].slice.call(arguments, 1)) : undefined;
};
