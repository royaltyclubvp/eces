App.Dictionary = {
    Registration: {
        emptyParticipationIdField: 'You must enter your participation ID before you can continue',
        confirmStart: 'The experiment is ready to begin. Please confirm when you are ready to start'
    }
};