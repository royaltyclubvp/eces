App.Collections.ShoppingCart = Backbone.Collection.extend({
    model: App.Models.Product,

    initialize: function() {
        this.priceTotal = 0;
        var scope = this;
        this.on('add', function(product) {
            product.set('in_cart', true);
            scope.priceTotal += parseFloat(product.get('price'));
            product.category.incrementProductCounter();
        });
        this.on('remove', function(product) {
            product.set('in_cart', false);
            scope.priceTotal -= parseFloat(product.get('price'));
            product.category.decrementProductCounter();
        });
        // Set Item Removal Check if Set to Force Cart Removal
        if (App.participant.experimentGroup.get('force_cart_removal')) {
            this.itemRemoved = false;
        }
    },

    getTotalPrice: function() {
        return this.priceTotal.toFixed(2);
    },

    checkIfItemRemoved: function() {
        return this.itemRemoved;
    },

    confirmItemRemoved: function() {
        this.itemRemoved = true;
    }
});
