App.Collections.ProductCatalogue = Backbone.Collection.extend({
    initialize: function(models, options) {
        if (models != null) {
            this.set(models);
        }
        if (options.url) {
            this.url = options.url
        }
        if (options.category) {
            this.category = options.category;
        }
    },

    //model: function(attrs, options) {
    //    if (this.category) {
    //        $.extend(attrs, {category: this.category.get('name')});
    //    }
    //    return new App.Models.Product(attrs, options)
    //}
    model: App.Models.Product
});
