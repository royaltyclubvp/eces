App.Collections.ExperimentCategoryViews = Backbone.Collection.extend({
    url: '/experiment_category_views',
    model: App.Models.ExperimentCategoryView
});
