App.Collections.ExperimentProductDetailViews = Backbone.Collection.extend({
    url: '/experiment_product_detail_views',
    model: App.Models.ExperimentProductDetailView
});
