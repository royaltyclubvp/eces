App.Collections.Experiments = Backbone.Collection.extend({
    model: App.Models.Experiment,
    url: apiUrl('experiments')
});
