App.Collections.ExperimentViews = Backbone.Collection.extend({
    url: '/experiment_product_views',
    model: App.Models.ExperimentView
});
