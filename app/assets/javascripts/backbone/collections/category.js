var app = app || {};

app.ProductCategory = Backbone.Collection.extend({
    model: app.Product,
    url: '/product_categories/'
});