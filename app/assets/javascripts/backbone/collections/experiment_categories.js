App.Collections.ExperimentCategories = Backbone.Collection.extend({
    model: App.Models.ProductCategory,

    comparator: function(model) {
        return model.get('order_no');
    },

    postSetupHook: function() {
        this.each(function(category) {
            category.postSetupHook();
        });
        this.currentModelIndex = -1;
    },

    currentCategory: function() {
        return (this.currentModelIndex >= 0) ? this.at(this.currentModelIndex) : false;
    },

    nextCategory: function() {
        return this.at(this.currentModelIndex + 1);
    },

    moveToNext: function() {
        this.currentModelIndex += 1;
    }
});
