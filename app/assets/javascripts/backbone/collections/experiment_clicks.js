App.Collections.ExperimentClicks = Backbone.Collection.extend({
    url: '/experiment_clicks',
    model: App.Models.ExperimentClick
});
