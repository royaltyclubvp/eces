App.Collections.ExperimentSelects = Backbone.Collection.extend({
    url: '/experiment_product_selects',
    model: App.Models.ExperimentSelect
});
