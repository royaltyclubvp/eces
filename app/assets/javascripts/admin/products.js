$(document).ready(function() {
    $('input[data-role=money]').autoNumeric('init');
    $('input[data-role=money]').keydown(function(event) {
        if (event.keyCode == 13) {
            $(this).autoNumeric('set', $(this).autoNumeric('get'));
        }
    });
});
