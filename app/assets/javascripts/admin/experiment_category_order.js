var App = App || {};

// Models
App.OrderCategory = Backbone.Model.extend({

});

// Collections
App.OrderCategories = Backbone.Collection.extend({
    model: App.OrderCategory,

    comparator: function(model) {
        return model.get('order_no');
    },

    initialize: function() {
        var scope = this;
        this.listenTo(this, 'add', function(model, collection, options) {
            scope.listenTo(model, 'change:order_no', scope.doesOrderNumberClash);
        });
    },

    doesOrderNumberClash: function(model, value, options) {
        var clash = false;
        this.each(function(category) {
            if (model != category) {
                if (category.get('order_no') == model.get('order_no')) {
                    category.set({order_no: model.previous('order_no')}, {silent: true});
                    clash = true;
                }
            }
        });
        if (clash) {
            this.sort();
            this.trigger('reset');
        }
    },

    save: function() {
        var scope = this;
        var params = [];
        for (var i=0; i < this.length; i++) {
            params.push(scope.models[i].toJSON());
        };
        var save = $.ajax({
            url: '/admin/save-group-category-order',
            dataType: 'json',
            data: {categories: params},
            method: 'POST'
        });
    }
});

App.orderCategories = new App.OrderCategories();

App.OrderCategoryTable = Backbone.View.extend({
    events: {
        'click #save-order-changes': 'saveNewOrder'
    },

    initialize: function() {
        if (this.$el.length > 0) {
            this.childViews = [];
            this.renderInitial();
            this.listenTo(Backbone, 'order_changed', this.showSaveButton);
            this.listenTo(App.orderCategories, 'reset', this.render);
            this.$contentContainer = this.$('tbody');
        }
    },

    renderInitial: function() {
        var scope = this;
        $('.category-order').each(function(index, el) {
            var view = new App.OrderCategoryView({el: el});
            view.pageLoadSetup();
            scope.childViews.push(view);
        });
    },

    render: function() {
        this.childViews = [];
        this.$contentContainer.empty();
        var container = document.createDocumentFragment();
        App.orderCategories.each(function(category) {
            var view = new App.OrderCategoryView({model: category});
            view.collectionResetSetup();
            container.appendChild(view.el);
            this.childViews.push(view);
        }, this);
        this.$contentContainer.html(container);
    },

    showSaveButton: function() {
        if (this.$('#save-order-changes').hasClass('hidden')) {
            this.$('#save-order-changes').removeClass('hidden');
        }
    },

    hideSaveButton: function() {
        if (!this.$('#save-order-changes').hasClass('hidden')) {
            this.$('#save-order-changes').addClass('hidden');
        }
    },

    saveNewOrder: function() {
        App.orderCategories.save();
        this.hideSaveButton();
    }
});

App.OrderCategoryView = Backbone.View.extend({
    className: 'category-order',
    tagName: 'tr',

    template: HandlebarsTemplates['categories/category_order'],

    events: {
        'click .save': 'saveNewOrder',
        //'keyup .category-order-input': 'handleChange',
        //'mouseup .category-order-input': 'handleChange',
        //'mousewheel .category-order-input': 'handleChange'
        'change .category-order-input': 'handleChange'
    },

    initialize: function() {
    },

    pageLoadSetup: function() {
        this.$orderInput = this.$('.category-order-input');
        this.model = new App.OrderCategory({
            name: this.$('.category-name').text(),
            order_no: this.$('.category-values .category-order-input').val(),
            experiment_group_category_id: this.$('.category-values .experiment-group-id').val()
        });
        App.orderCategories.add(this.model);
    },

    collectionResetSetup: function() {
        this.render();
        this.$orderInput = this.$('.category-order-input');
    },

    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
    },

    handleChange: function() {
        var newValue = this.$orderInput.val();
        this.model.set('order_no', newValue);
        Backbone.trigger('order_changed');
    },
});

$(document).ready(function() {
    App.categoryTable = new App.OrderCategoryTable({el: '#category-order-table'});
});
