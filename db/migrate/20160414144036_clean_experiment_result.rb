class CleanExperimentResult < ActiveRecord::Migration
  def change
    ExperimentResult.find_each do |result|
      if result.participant.nil?
        result.destroy
      elsif result.participant.experiment_group.nil?
        result.participant.destroy
      end
    end
  end
end
