class AddViewTimeToExperimentProductViews < ActiveRecord::Migration
  def change
    add_column :experiment_product_views, :view_time, :integer
  end
end
