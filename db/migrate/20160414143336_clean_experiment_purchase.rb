class CleanExperimentPurchase < ActiveRecord::Migration
  def change
    ExperimentPurchase.find_each do |purchase|
      if purchase.participant.nil?
        purchase.destroy
      elsif purchase.participant.experiment_group.nil?
        purchase.participant.destroy
      else
        purchase.experiment_id = purchase.participant.experiment_group.experiment_id
        purchase.save
      end
    end
  end
end
