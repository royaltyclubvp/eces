class AddExperimentIdToExperimentProductPlacement < ActiveRecord::Migration
  def change
    add_column :experiment_product_placements, :experiment_id, :integer
    add_index :experiment_product_placements, :experiment_id
  end
end
