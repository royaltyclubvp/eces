class AddClickTimeToExperimentClicks < ActiveRecord::Migration
  def change
    add_column :experiment_clicks, :click_time, :integer
  end
end
