class AddTokenToParticipant < ActiveRecord::Migration
  def change
    add_column :participants, :access_token, :string
    add_index :participants, :access_token
  end
end
