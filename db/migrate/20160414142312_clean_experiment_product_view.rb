class CleanExperimentProductView < ActiveRecord::Migration
  def change
    ExperimentProductView.find_each do |view|
      if view.participant.nil?
        view.destroy
      elsif view.participant.experiment_group.nil?
        view.participant.destroy
      else
        view.experiment_id = view.participant.experiment_group.experiment_id
        view.save
      end
    end
  end
end
