class CleanParticipantExperimentIds < ActiveRecord::Migration
  def change
    Participant.find_each do |p|
      if p.experiment_group.nil?
        p.destroy
      elsif p.experiment.nil?
        p.experiment_id = p.experiment_group.experiment_id
      end
    end
  end
end
