class CleanParticipantClick < ActiveRecord::Migration
  def change
    ExperimentClick.find_each do |click|
      if click.participant.nil?
        click.destroy
      end
    end
  end
end
