class CreateExperimentCategoryViews < ActiveRecord::Migration
  def change
    create_table :experiment_category_views do |t|
      t.integer :category_id
      t.integer :participant_id
      t.integer :experiment_id
      t.integer :view_time

      t.timestamps null: false
    end
  end
end
