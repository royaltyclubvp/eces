class AddExperimentIdToExperimentProductSelect < ActiveRecord::Migration
  def change
    add_column :experiment_product_selects, :experiment_id, :integer
    add_index :experiment_product_selects, :experiment_id
  end
end
