class CleanExperimentResults < ActiveRecord::Migration
  def change
    ExperimentResult.find_each do |result|
      if result.experiment_group.nil?
        result.experiment_group_id = result.participant.experiment_group_id
        result.save
      end
    end
  end
end
