class CleanExperimentClick < ActiveRecord::Migration
  def change
    ExperimentClick.find_each do |click|
      if click.participant.nil?
        click.destroy
      elsif click.participant.experiment_group.nil?
        click.participant.destroy
      else
        click.experiment_id = click.participant.experiment_group.experiment_id
        click.save
      end
    end
  end
end
