class CleanParticipantResult < ActiveRecord::Migration
  def change
    Participant.find_each do |p|
      if p.experiment_result.nil?
        p.destroy
      end
    end
  end
end
