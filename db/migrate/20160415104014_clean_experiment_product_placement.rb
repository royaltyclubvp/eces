class CleanExperimentProductPlacement < ActiveRecord::Migration
  def change
    ExperimentProductPlacement.find_each do |placement|
      if placement.participant.nil?
        placement.destroy
      elsif placement.participant.experiment_group.nil?
        placement.participant.destroy
      else
        placement.experiment_id = placement.participant.experiment_group.experiment_id
        placement.save
      end
    end
  end
end
