class AddExperimentIdToExperimentPurchase < ActiveRecord::Migration
  def change
    add_column :experiment_purchases, :experiment_id, :integer
    add_index :experiment_purchases, :experiment_id
  end
end
