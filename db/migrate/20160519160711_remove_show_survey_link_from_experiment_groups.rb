class RemoveShowSurveyLinkFromExperimentGroups < ActiveRecord::Migration
  def change
    remove_column :experiment_groups, :show_survey_link, :boolean
  end
end
