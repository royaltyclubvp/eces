class AddSelectTimeToExperimentProductSelects < ActiveRecord::Migration
  def change
    add_column :experiment_product_selects, :select_time, :integer
  end
end
