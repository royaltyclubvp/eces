class AddInappropriateStartPointMessageToExperimentGroup < ActiveRecord::Migration
  def change
    add_column :experiment_groups, :inappropriate_start_point_message, :string
  end
end
