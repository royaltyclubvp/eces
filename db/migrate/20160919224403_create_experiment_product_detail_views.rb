class CreateExperimentProductDetailViews < ActiveRecord::Migration
  def change
    create_table :experiment_product_detail_views do |t|
      t.integer :product_id
      t.integer :participant_id
      t.integer :experiment_id
      t.integer :view_time

      t.timestamps null: false
    end
  end
end
