class AddExperimentIdToParticipant < ActiveRecord::Migration
  def change
    add_column :participants, :experiment_id, :integer
    add_index :participants, :experiment_id
  end
end
