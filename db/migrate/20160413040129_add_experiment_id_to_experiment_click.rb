class AddExperimentIdToExperimentClick < ActiveRecord::Migration
  def change
    add_column :experiment_clicks, :experiment_id, :integer
    add_index :experiment_clicks, :experiment_id
  end
end