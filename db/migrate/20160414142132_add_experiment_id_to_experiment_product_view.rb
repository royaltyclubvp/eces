class AddExperimentIdToExperimentProductView < ActiveRecord::Migration
  def change
    add_column :experiment_product_views, :experiment_id, :integer
    add_index :experiment_product_views, :experiment_id
  end
end
