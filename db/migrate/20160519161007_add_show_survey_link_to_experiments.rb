class AddShowSurveyLinkToExperiments < ActiveRecord::Migration
  def change
    add_column :experiments, :show_survey_link, :boolean
  end
end
