class AddOrderNoToExperimentGroupCategory < ActiveRecord::Migration
  def change
    add_column :experiment_group_categories, :order_no, :integer
  end
end
