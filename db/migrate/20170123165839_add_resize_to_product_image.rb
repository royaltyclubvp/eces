class AddResizeToProductImage < ActiveRecord::Migration
  def change
    add_column :product_images, :resize, :boolean, default: false
  end
end
