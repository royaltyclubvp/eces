class CleanParticipants < ActiveRecord::Migration
  def change
    ExperimentProductPlacement.find_each do |place|
      if place.participant.nil?
        place.destroy
      end
    end
    ExperimentProductSelect.find_each do |select|
      if select.participant.nil?
        select.destroy
      end
    end
    ExperimentProductView.find_each do |view|
      if select.participant.nil?
        view.destroy
      end
    end
    ExperimentPurchase.find_each do |purchase|
      if purchase.participant.nil?
        purchase.destroy
      end
    end
    ExperimentResult.find_each do |result|
      if result.participant.nil?
        result.destroy
      end
    end
  end
end
