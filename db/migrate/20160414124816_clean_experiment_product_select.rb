class CleanExperimentProductSelect < ActiveRecord::Migration
  def change
    ExperimentProductSelect.find_each do |select|
      if select.participant.nil?
        select.destroy
      elsif select.participant.experiment_group.nil?
        select.participant.destroy
      else
        select.experiment_id = select.participant.experiment_group.experiment_id
        select.save
      end
    end
  end
end
