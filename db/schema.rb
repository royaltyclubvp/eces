# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170123165839) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
  add_index "admins", ["unlock_token"], name: "index_admins_on_unlock_token", unique: true, using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories_experiment_groups", force: :cascade do |t|
    t.integer  "experiment_group_id"
    t.integer  "category_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "experiment_category_views", force: :cascade do |t|
    t.integer  "category_id"
    t.integer  "participant_id"
    t.integer  "experiment_id"
    t.integer  "view_time"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "experiment_clicks", force: :cascade do |t|
    t.integer  "participant_id"
    t.string   "click_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "experiment_id"
    t.integer  "click_time"
  end

  add_index "experiment_clicks", ["experiment_id"], name: "index_experiment_clicks_on_experiment_id", using: :btree

  create_table "experiment_group_categories", force: :cascade do |t|
    t.integer  "experiment_group_id"
    t.integer  "category_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "order_no"
  end

  create_table "experiment_groups", force: :cascade do |t|
    t.integer  "group_no"
    t.integer  "experiment_id"
    t.boolean  "step_by_step"
    t.integer  "products_per_page"
    t.string   "price_location"
    t.string   "add_to_basket_location"
    t.boolean  "detail_page"
    t.string   "basket_order"
    t.integer  "category_selection_limit"
    t.boolean  "out_of_stock"
    t.boolean  "randomization_of_products"
    t.string   "cart_position"
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.string   "background_type"
    t.string   "background_color"
    t.string   "background_image"
    t.string   "logo_filename"
    t.string   "front_image_1"
    t.string   "front_image_2"
    t.string   "front_image_3"
    t.string   "slogan"
    t.boolean  "out_of_stock_per_category"
    t.string   "out_of_stock_message"
    t.string   "step_by_step_inappropriate_category_message"
    t.boolean  "force_cart_removal",                          default: false
    t.string   "force_cart_removal_message"
    t.string   "category_limit_message"
    t.string   "font_color"
    t.string   "step_by_step_no_selection_message"
    t.string   "inappropriate_start_point_message"
  end

  create_table "experiment_product_detail_views", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "participant_id"
    t.integer  "experiment_id"
    t.integer  "view_time"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "experiment_product_placements", force: :cascade do |t|
    t.integer  "product_id"
    t.string   "grid_location"
    t.integer  "participant_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "experiment_id"
  end

  add_index "experiment_product_placements", ["experiment_id"], name: "index_experiment_product_placements_on_experiment_id", using: :btree

  create_table "experiment_product_selects", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "participant_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "experiment_id"
    t.integer  "select_time"
  end

  add_index "experiment_product_selects", ["experiment_id"], name: "index_experiment_product_selects_on_experiment_id", using: :btree

  create_table "experiment_product_views", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "participant_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "experiment_id"
    t.integer  "view_time"
  end

  add_index "experiment_product_views", ["experiment_id"], name: "index_experiment_product_views_on_experiment_id", using: :btree

  create_table "experiment_purchases", force: :cascade do |t|
    t.integer  "participant_id"
    t.integer  "product_id"
    t.integer  "quantity"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "experiment_id"
  end

  add_index "experiment_purchases", ["experiment_id"], name: "index_experiment_purchases_on_experiment_id", using: :btree

  create_table "experiment_results", force: :cascade do |t|
    t.integer  "experiment_id"
    t.integer  "experiment_group_id"
    t.integer  "participant_id"
    t.boolean  "completed"
    t.integer  "completion_time"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "experiments", force: :cascade do |t|
    t.string   "name"
    t.boolean  "available"
    t.string   "survey_link"
    t.integer  "experiment_group_counter", default: 0
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "completion_message"
    t.boolean  "show_survey_link"
  end

  create_table "participants", force: :cascade do |t|
    t.string   "participation_id"
    t.integer  "experiment_group_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "experiment_id"
    t.string   "access_token"
  end

  add_index "participants", ["access_token"], name: "index_participants_on_access_token", using: :btree
  add_index "participants", ["experiment_id"], name: "index_participants_on_experiment_id", using: :btree

  create_table "product_images", force: :cascade do |t|
    t.string   "filename"
    t.integer  "product_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "resize",     default: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "title"
    t.integer  "price_cents"
    t.text     "description"
    t.integer  "category_id"
    t.integer  "main_product_image_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

end
