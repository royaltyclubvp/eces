# == Route Map
#
#                                 Prefix Verb   URI Pattern                                                                                 Controller#Action
#                                   root GET    /                                                                                           experiment#home2
#                      select_experiment GET    /select-experiment(.:format)                                                                experiment#home2
#                                   home GET    /home(.:format)                                                                             experiment#home2
#              experiment_results_create GET    /experiment_results/create(.:format)                                                        experiment_results#create
#                       purchases_create GET    /purchases/create(.:format)                                                                 purchases#create
#      experiment_product_selects_create GET    /experiment_product_selects/create(.:format)                                                experiment_product_selects#create
#               experiment_clicks_create GET    /experiment_clicks/create(.:format)                                                         experiment_clicks#create
#               product_categories_index GET    /product_categories/index(.:format)                                                         product_categories#index
#                      experiment_groups GET    /experiment_groups(.:format)                                                                experiment_groups#index
#                                        POST   /experiment_groups(.:format)                                                                experiment_groups#create
#                   new_experiment_group GET    /experiment_groups/new(.:format)                                                            experiment_groups#new
#                  edit_experiment_group GET    /experiment_groups/:id/edit(.:format)                                                       experiment_groups#edit
#                       experiment_group GET    /experiment_groups/:id(.:format)                                                            experiment_groups#show
#                                        PATCH  /experiment_groups/:id(.:format)                                                            experiment_groups#update
#                                        PUT    /experiment_groups/:id(.:format)                                                            experiment_groups#update
#                                        DELETE /experiment_groups/:id(.:format)                                                            experiment_groups#destroy
#                      experiment_clicks GET    /experiment_clicks(.:format)                                                                experiment_clicks#index
#                                        POST   /experiment_clicks(.:format)                                                                experiment_clicks#create
#                   new_experiment_click GET    /experiment_clicks/new(.:format)                                                            experiment_clicks#new
#                  edit_experiment_click GET    /experiment_clicks/:id/edit(.:format)                                                       experiment_clicks#edit
#                       experiment_click GET    /experiment_clicks/:id(.:format)                                                            experiment_clicks#show
#                                        PATCH  /experiment_clicks/:id(.:format)                                                            experiment_clicks#update
#                                        PUT    /experiment_clicks/:id(.:format)                                                            experiment_clicks#update
#                                        DELETE /experiment_clicks/:id(.:format)                                                            experiment_clicks#destroy
#             experiment_product_selects GET    /experiment_product_selects(.:format)                                                       experiment_product_selects#index
#                                        POST   /experiment_product_selects(.:format)                                                       experiment_product_selects#create
#          new_experiment_product_select GET    /experiment_product_selects/new(.:format)                                                   experiment_product_selects#new
#         edit_experiment_product_select GET    /experiment_product_selects/:id/edit(.:format)                                              experiment_product_selects#edit
#              experiment_product_select GET    /experiment_product_selects/:id(.:format)                                                   experiment_product_selects#show
#                                        PATCH  /experiment_product_selects/:id(.:format)                                                   experiment_product_selects#update
#                                        PUT    /experiment_product_selects/:id(.:format)                                                   experiment_product_selects#update
#                                        DELETE /experiment_product_selects/:id(.:format)                                                   experiment_product_selects#destroy
#               experiment_product_views GET    /experiment_product_views(.:format)                                                         experiment_product_views#index
#                                        POST   /experiment_product_views(.:format)                                                         experiment_product_views#create
#            new_experiment_product_view GET    /experiment_product_views/new(.:format)                                                     experiment_product_views#new
#           edit_experiment_product_view GET    /experiment_product_views/:id/edit(.:format)                                                experiment_product_views#edit
#                experiment_product_view GET    /experiment_product_views/:id(.:format)                                                     experiment_product_views#show
#                                        PATCH  /experiment_product_views/:id(.:format)                                                     experiment_product_views#update
#                                        PUT    /experiment_product_views/:id(.:format)                                                     experiment_product_views#update
#                                        DELETE /experiment_product_views/:id(.:format)                                                     experiment_product_views#destroy
#                              purchases GET    /purchases(.:format)                                                                        purchases#index
#                                        POST   /purchases(.:format)                                                                        purchases#create
#                           new_purchase GET    /purchases/new(.:format)                                                                    purchases#new
#                          edit_purchase GET    /purchases/:id/edit(.:format)                                                               purchases#edit
#                               purchase GET    /purchases/:id(.:format)                                                                    purchases#show
#                                        PATCH  /purchases/:id(.:format)                                                                    purchases#update
#                                        PUT    /purchases/:id(.:format)                                                                    purchases#update
#                                        DELETE /purchases/:id(.:format)                                                                    purchases#destroy
#                     experiment_results GET    /experiment_results(.:format)                                                               experiment_results#index
#                                        POST   /experiment_results(.:format)                                                               experiment_results#create
#                  new_experiment_result GET    /experiment_results/new(.:format)                                                           experiment_results#new
#                 edit_experiment_result GET    /experiment_results/:id/edit(.:format)                                                      experiment_results#edit
#                      experiment_result GET    /experiment_results/:id(.:format)                                                           experiment_results#show
#                                        PATCH  /experiment_results/:id(.:format)                                                           experiment_results#update
#                                        PUT    /experiment_results/:id(.:format)                                                           experiment_results#update
#                                        DELETE /experiment_results/:id(.:format)                                                           experiment_results#destroy
#                       experiment_index GET    /experiment(.:format)                                                                       experiment#index
#                           participants POST   /participants(.:format)                                                                     participants#create
#         edit_experiment_result_package GET    /experiment_result_package/:id/edit(.:format)                                               experiment_result_package#edit
#                                        POST   /participants/:access_token/select-experiment(.:format)                                     participants#select_experiment
#                                        GET    /product_categories/:id/:access_token(.:format)                                             product_categories#show
#                        experiment_home GET    /experiment/home(.:format)                                                                  experiment#home
#                        new_participant POST   /new_participant(.:format)                                                                  sessions#create
#                      end_participation DELETE /end_participation(.:format)                                                                sessions#destroy
#                                        GET    /select_experiment(.:format)                                                                sessions#select_experiment
#                assign_experiment_group POST   /assign_experiment_group(.:format)                                                          sessions#assign_experiment_group
#                        assign_to_group GET    /assign_experiment_group/:experiment_id(.:format)                                           sessions#assign_experiment_group
#                      new_admin_session GET    /admin/login(.:format)                                                                      admins/sessions#new
#                          admin_session POST   /admin/login(.:format)                                                                      admins/sessions#create
#                  destroy_admin_session DELETE /admin/sign_out(.:format)                                                                   admins/sessions#destroy
#                         admin_password POST   /admin/password(.:format)                                                                   devise/passwords#create
#                     new_admin_password GET    /admin/password/new(.:format)                                                               devise/passwords#new
#                    edit_admin_password GET    /admin/password/edit(.:format)                                                              devise/passwords#edit
#                                        PATCH  /admin/password(.:format)                                                                   devise/passwords#update
#                                        PUT    /admin/password(.:format)                                                                   devise/passwords#update
#              cancel_admin_registration GET    /admin/cancel(.:format)                                                                     admins/registrations#cancel
#                     admin_registration POST   /admin(.:format)                                                                            admins/registrations#create
#                 new_admin_registration GET    /admin/sign_up(.:format)                                                                    admins/registrations#new
#                edit_admin_registration GET    /admin/edit(.:format)                                                                       admins/registrations#edit
#                                        PATCH  /admin(.:format)                                                                            admins/registrations#update
#                                        PUT    /admin(.:format)                                                                            admins/registrations#update
#                                        DELETE /admin(.:format)                                                                            admins/registrations#destroy
#                           admin_unlock POST   /admin/unlock(.:format)                                                                     devise/unlocks#create
#                       new_admin_unlock GET    /admin/unlock/new(.:format)                                                                 devise/unlocks#new
#                                        GET    /admin/unlock(.:format)                                                                     devise/unlocks#show
#                             admin_root GET    /admin(.:format)                                                                            admin/static_pages#index
#          admin_experiment_results_home GET    /admin/experiment_results/home(.:format)                                                    admin/experiment_results#home
#    admin_experiment_results_home_index GET    /admin/experiment_results/index/experiment/:id(.:format)                                    admin/experiment_results#index
#     admin_experiment_results_purchases GET    /admin/experiment_results/purchases/participant/:id(.:format)                               admin/experiment_results#purchases
#     admin_experiment_experiment_groups GET    /admin/experiments/:experiment_id/experiment_groups(.:format)                               admin/experiment_groups#index
#                                        POST   /admin/experiments/:experiment_id/experiment_groups(.:format)                               admin/experiment_groups#create
#  new_admin_experiment_experiment_group GET    /admin/experiments/:experiment_id/experiment_groups/new(.:format)                           admin/experiment_groups#new
# edit_admin_experiment_experiment_group GET    /admin/experiments/:experiment_id/experiment_groups/:id/edit(.:format)                      admin/experiment_groups#edit
#      admin_experiment_experiment_group GET    /admin/experiments/:experiment_id/experiment_groups/:id(.:format)                           admin/experiment_groups#show
#                                        PATCH  /admin/experiments/:experiment_id/experiment_groups/:id(.:format)                           admin/experiment_groups#update
#                                        PUT    /admin/experiments/:experiment_id/experiment_groups/:id(.:format)                           admin/experiment_groups#update
#                                        DELETE /admin/experiments/:experiment_id/experiment_groups/:id(.:format)                           admin/experiment_groups#destroy
#                      admin_experiments GET    /admin/experiments(.:format)                                                                admin/experiments#index
#                                        POST   /admin/experiments(.:format)                                                                admin/experiments#create
#                   new_admin_experiment GET    /admin/experiments/new(.:format)                                                            admin/experiments#new
#                  edit_admin_experiment GET    /admin/experiments/:id/edit(.:format)                                                       admin/experiments#edit
#                       admin_experiment GET    /admin/experiments/:id(.:format)                                                            admin/experiments#show
#                                        PATCH  /admin/experiments/:id(.:format)                                                            admin/experiments#update
#                                        PUT    /admin/experiments/:id(.:format)                                                            admin/experiments#update
#                                        DELETE /admin/experiments/:id(.:format)                                                            admin/experiments#destroy
#        admin_make_experiment_available PUT    /admin/experiments/:id/make_available(.:format)                                             admin/experiments#make_available
#      admin_make_experiment_unavailable PUT    /admin/experiments/:id/make_unavailable(.:format)                                           admin/experiments#make_unavailable
#                admin_experiment_groups GET    /admin/experiment_groups(.:format)                                                          admin/experiment_groups#index
#                                        POST   /admin/experiment_groups(.:format)                                                          admin/experiment_groups#create
#             new_admin_experiment_group GET    /admin/experiment_groups/new(.:format)                                                      admin/experiment_groups#new
#            edit_admin_experiment_group GET    /admin/experiment_groups/:id/edit(.:format)                                                 admin/experiment_groups#edit
#                 admin_experiment_group GET    /admin/experiment_groups/:id(.:format)                                                      admin/experiment_groups#show
#                                        PATCH  /admin/experiment_groups/:id(.:format)                                                      admin/experiment_groups#update
#                                        PUT    /admin/experiment_groups/:id(.:format)                                                      admin/experiment_groups#update
#                                        DELETE /admin/experiment_groups/:id(.:format)                                                      admin/experiment_groups#destroy
#                admin_add_to_experiment GET    /admin/experiment_groups/:experiment_id/add_to_experiment(.:format)                         admin/experiment_groups#add_to_experiment
#           admin_show_experiment_groups GET    /admin/experiment_groups/:experiment_id/show_groups(.:format)                               admin/experiment_groups#index
#               admin_add_all_categories GET    /admin/experiment_groups/:id/add_all_categories(.:format)                                   admin/experiment_groups#add_all_categories
#            admin_delete_all_categories GET    /admin/experiment_groups/:id/delete_all_categories(.:format)                                admin/experiment_groups#delete_all_categories
#                 admin_destroy_category GET    /admin/experiment_groups/group/:group_id/category/:category_id/remove_category(.:format)    admin/experiment_groups#destroy_category
#                     admin_add_category GET    /admin/experiment_groups/group/:group_id/category/:category_id/add_category(.:format)       admin/experiment_groups#add_category
#    admin_category_products_by_category GET    /admin/categories/:category_id/products_by_category(.:format)                               admin/products#index_by_category
#                admin_category_products GET    /admin/categories/:category_id/products(.:format)                                           admin/products#index
#                                        POST   /admin/categories/:category_id/products(.:format)                                           admin/products#create
#             new_admin_category_product GET    /admin/categories/:category_id/products/new(.:format)                                       admin/products#new
#            edit_admin_category_product GET    /admin/categories/:category_id/products/:id/edit(.:format)                                  admin/products#edit
#                 admin_category_product GET    /admin/categories/:category_id/products/:id(.:format)                                       admin/products#show
#                                        PATCH  /admin/categories/:category_id/products/:id(.:format)                                       admin/products#update
#                                        PUT    /admin/categories/:category_id/products/:id(.:format)                                       admin/products#update
#                                        DELETE /admin/categories/:category_id/products/:id(.:format)                                       admin/products#destroy
#                       admin_categories GET    /admin/categories(.:format)                                                                 admin/categories#index
#                                        POST   /admin/categories(.:format)                                                                 admin/categories#create
#                     new_admin_category GET    /admin/categories/new(.:format)                                                             admin/categories#new
#                    edit_admin_category GET    /admin/categories/:id/edit(.:format)                                                        admin/categories#edit
#                         admin_category GET    /admin/categories/:id(.:format)                                                             admin/categories#show
#                                        PATCH  /admin/categories/:id(.:format)                                                             admin/categories#update
#                                        PUT    /admin/categories/:id(.:format)                                                             admin/categories#update
#                                        DELETE /admin/categories/:id(.:format)                                                             admin/categories#destroy
#           admin_product_product_images GET    /admin/products/:product_id/product_images(.:format)                                        admin/product_images#index
#                                        POST   /admin/products/:product_id/product_images(.:format)                                        admin/product_images#create
#        new_admin_product_product_image GET    /admin/products/:product_id/product_images/new(.:format)                                    admin/product_images#new
#       edit_admin_product_product_image GET    /admin/products/:product_id/product_images/:id/edit(.:format)                               admin/product_images#edit
#            admin_product_product_image GET    /admin/products/:product_id/product_images/:id(.:format)                                    admin/product_images#show
#                                        PATCH  /admin/products/:product_id/product_images/:id(.:format)                                    admin/product_images#update
#                                        PUT    /admin/products/:product_id/product_images/:id(.:format)                                    admin/product_images#update
#                                        DELETE /admin/products/:product_id/product_images/:id(.:format)                                    admin/product_images#destroy
#                         admin_products GET    /admin/products(.:format)                                                                   admin/products#index
#                                        POST   /admin/products(.:format)                                                                   admin/products#create
#                      new_admin_product GET    /admin/products/new(.:format)                                                               admin/products#new
#                     edit_admin_product GET    /admin/products/:id/edit(.:format)                                                          admin/products#edit
#                          admin_product GET    /admin/products/:id(.:format)                                                               admin/products#show
#                                        PATCH  /admin/products/:id(.:format)                                                               admin/products#update
#                                        PUT    /admin/products/:id(.:format)                                                               admin/products#update
#                                        DELETE /admin/products/:id(.:format)                                                               admin/products#destroy
#                    admin_home_products GET    /admin/product-categories(.:format)                                                         admin/products#product_categories_home
#                   admin_product_images GET    /admin/product_images(.:format)                                                             admin/product_images#index
#                                        POST   /admin/product_images(.:format)                                                             admin/product_images#create
#                new_admin_product_image GET    /admin/product_images/new(.:format)                                                         admin/product_images#new
#               edit_admin_product_image GET    /admin/product_images/:id/edit(.:format)                                                    admin/product_images#edit
#                    admin_product_image GET    /admin/product_images/:id(.:format)                                                         admin/product_images#show
#                                        PATCH  /admin/product_images/:id(.:format)                                                         admin/product_images#update
#                                        PUT    /admin/product_images/:id(.:format)                                                         admin/product_images#update
#                                        DELETE /admin/product_images/:id(.:format)                                                         admin/product_images#destroy
#                admin_new_product_image GET    /admin/product_images/:product_id/new(.:format)                                             admin/product_images#new
#                  admin_make_main_image GET    /admin/product_images/make_main_image/product/:product_id/image/:product_image_id(.:format) admin/product_images#make_main_image
#

Rails.application.routes.draw do

  get 'experiment_result_package/edit'

  root 'experiment#home2'

  get 'select-experiment' => 'experiment#home2'

  get 'home' => 'experiment#home2'

  get 'experiment_results/create'

  get 'purchases/create'

  get 'product_categories/index'

  resources :experiment_groups
  resources :experiment_clicks
  resources :experiment_product_selects
  resources :experiment_product_views
  resources :experiment_category_views
  resources :experiment_product_detail_views
  #resources :product_categories
  resources :purchases
  resources :experiment_results
  resources :experiment, only: [:index]
  resources :participants, only: [:create]
  resources :experiment_result_package, only: [:update]

  post 'participants/:access_token/select-experiment' => 'participants#select_experiment'
  get 'product_categories/:id/:access_token' => 'product_categories#show'

  get 'experiment/home'

  post   'new_participant'   => 'sessions#create'
  delete 'end_participation'  => 'sessions#destroy'

  get 'select_experiment'     => 'sessions#select_experiment'
  post 'assign_experiment_group' =>  'sessions#assign_experiment_group'
  get 'assign_experiment_group/:experiment_id' => 'sessions#assign_experiment_group', as: 'assign_to_group'

  devise_for :admins,
             controllers: {
                 sessions: 'admins/sessions',
                 registrations: 'admins/registrations'
             },
             path: '/admin',
             path_names: {
                 sign_in: 'login'
             }
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  namespace :admin do
    root :to => "static_pages#index"
    get 'experiment_results/home'
    get 'experiment_results/index/experiment/:id'   => 'experiment_results#index', as: :experiment_results_home_index
    delete 'experiment_results/index/experiment/:id' => 'experiment_results#destroy', as: :experiment_results_home_destroy 
    get 'experiment_results/purchases/participant/:id' => 'experiment_results#purchases', as: :experiment_results_purchases
    resources :experiments do
      resources :experiment_groups
    end
    put 'experiments/:id/make_available/' => 'experiments#make_available', as: :make_experiment_available
    put 'experiments/:id/make_unavailable' => 'experiments#make_unavailable', as: :make_experiment_unavailable
    resources :experiment_groups
    get 'experiment_groups/:experiment_id/add_to_experiment' => 'experiment_groups#add_to_experiment', as: :add_to_experiment
    get 'experiment_groups/:experiment_id/show_groups' => 'experiment_groups#index', as: :show_experiment_groups
    get 'experiment_groups/:id/add_all_categories' => 'experiment_groups#add_all_categories', as: :add_all_categories
    get 'experiment_groups/:id/delete_all_categories' => 'experiment_groups#delete_all_categories', as: :delete_all_categories
    get 'experiment_groups/group/:group_id/category/:category_id/remove_category' => 'experiment_groups#destroy_category', as: :destroy_category
    get 'experiment_groups/group/:group_id/category/:category_id/add_category' => 'experiment_groups#add_category', as: :add_category
    resources :categories do
      get 'products_by_category' => 'products#index_by_category'
      resources :products
    end
    resources :products do
      resources :product_images
    end
    get 'product-categories/' => 'products#product_categories_home', as: :home_products
    resources :product_images
    get 'product_images/:product_id/new' => 'product_images#new', as: :new_product_image
    get 'product_images/make_main_image/product/:product_id/image/:product_image_id' => 'product_images#make_main_image', as: :make_main_image
    post 'save-group-category-order' => 'experiment_group_categories#save_group_category_order'
  end

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
